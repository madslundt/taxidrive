using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Shouldly;
using System;
using System.Collections.Generic;
using Source.Features.Review;

namespace UnitTests.Features.Review
{

    public class GetDriverToCustomerReviewsTests
    {
        private readonly IMediator _mediator;
        public GetDriverToCustomerReviewsTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
            {
                DriverId = Guid.Empty,
                CustomerId = Guid.NewGuid()
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
            {
                DriverId = Guid.NewGuid(),
                CustomerId = Guid.Empty
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenDriverDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.CustomerContext.Customer Customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(Customer);
                await db.SaveChangesAsync();

                GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
                {
                    DriverId = Guid.NewGuid(),
                    CustomerId = Customer.Id
                };

                GetDriverToCustomerReviews.Handler handler = new GetDriverToCustomerReviews.Handler(db);

                ICollection<GetDriverToCustomerReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCustomerDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.DriverContext.Driver Driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(Driver);
                await db.SaveChangesAsync();

                GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
                {
                    DriverId = Driver.Id,
                    CustomerId = Guid.NewGuid()
                };

                GetDriverToCustomerReviews.Handler handler = new GetDriverToCustomerReviews.Handler(db);

                ICollection<GetDriverToCustomerReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitLessThan1()
        {
            GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
            {
                DriverId = Guid.NewGuid(),
                Limit = 0
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitGreaterThan100()
        {
            GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
            {
                DriverId = Guid.NewGuid(),
                Limit = 101
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithOffsetLessThan0()
        {
            GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
            {
                DriverId = Guid.NewGuid(),
                Offset = -1
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCustomerHaveNoTripsWithDriver()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.DriverContext.Driver Driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.CustomerContext.Customer Customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(Driver);
                db.Add(Customer);
                await db.SaveChangesAsync();

                GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
                {
                    DriverId = Driver.Id,
                    CustomerId = Customer.Id
                };

                GetDriverToCustomerReviews.Handler handler = new GetDriverToCustomerReviews.Handler(db);

                ICollection<GetDriverToCustomerReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }

        [Fact]
        public async Task ItShouldReturnReviewsWhenCustomerHaveReviewsFromDriver()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.DriverContext.Driver Driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.CustomerContext.Customer Customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.DriverToCustomerReviewContext.DriverToCustomerReview review = new Faker<DataModels.DriverToCustomerReviewContext.DriverToCustomerReview>()
                    .RuleFor(c => c.Customer, f => Customer)
                    .RuleFor(c => c.Creator, f => Driver)
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .RuleFor(c => c.TripId, f => Guid.NewGuid())
                    .Generate();


                db.Add(Driver);
                db.Add(Customer);
                db.Add(review);
                await db.SaveChangesAsync();

                GetDriverToCustomerReviews.Request request = new GetDriverToCustomerReviews.Request
                {
                    DriverId = Driver.Id,
                    CustomerId = Customer.Id
                };

                GetDriverToCustomerReviews.Handler handler = new GetDriverToCustomerReviews.Handler(db);

                ICollection<GetDriverToCustomerReviews.Response> actualReviews = await handler.Handle(request).ConfigureAwait(false);

                foreach (GetDriverToCustomerReviews.Response actualReview in actualReviews)
                {
                    actualReview.Id.ShouldBe(review.Id);
                    actualReview.Rating.ShouldBe(review.Rating);
                    actualReview.TripId.ShouldBe(review.TripId);
                    actualReview.Created.ShouldBe(review.Created);
                    actualReview.Text.ShouldBe(review.Text);
                }
            }
        }
    }
}
