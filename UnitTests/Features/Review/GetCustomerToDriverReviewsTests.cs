using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Shouldly;
using System;
using System.Collections.Generic;
using Source.Features.Review;

namespace UnitTests.Features.Review
{

    public class GetCustomerToDriverReviewsTests
    {
        private readonly IMediator _mediator;
        public GetCustomerToDriverReviewsTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
            {
                CustomerId = Guid.Empty,
                DriverId = Guid.NewGuid()
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
            {
                CustomerId = Guid.NewGuid(),
                DriverId = Guid.Empty
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenCustomerDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(driver);
                await db.SaveChangesAsync();

                GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
                {
                    CustomerId = Guid.NewGuid(),
                    DriverId = driver.Id
                };

                GetCustomerToDriverReviews.Handler handler = new GetCustomerToDriverReviews.Handler(db);

                ICollection<GetCustomerToDriverReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenDriverDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(customer);
                await db.SaveChangesAsync();

                GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
                {
                    CustomerId = customer.Id,
                    DriverId = Guid.NewGuid()
                };

                GetCustomerToDriverReviews.Handler handler = new GetCustomerToDriverReviews.Handler(db);

                ICollection<GetCustomerToDriverReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitLessThan1()
        {
            GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
            {
                CustomerId = Guid.NewGuid(),
                Limit = 0
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitGreaterThan100()
        {
            GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
            {
                CustomerId = Guid.NewGuid(),
                Limit = 101
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithOffsetLessThan0()
        {
            GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
            {
                CustomerId = Guid.NewGuid(),
                Offset = -1
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenDriverHaveNoTripsWithCustomer()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(customer);
                db.Add(driver);
                await db.SaveChangesAsync();

                GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
                {
                    CustomerId = customer.Id,
                    DriverId = driver.Id
                };

                GetCustomerToDriverReviews.Handler handler = new GetCustomerToDriverReviews.Handler(db);

                ICollection<GetCustomerToDriverReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }

        [Fact]
        public async Task ItShouldReturnReviewsWhenDriverHaveReviewsFromCustomer()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.CustomerToDriverReviewContext.CustomerToDriverReview review = new Faker<DataModels.CustomerToDriverReviewContext.CustomerToDriverReview>()
                    .RuleFor(c => c.Driver, f => driver)
                    .RuleFor(c => c.Creator, f => customer)
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .RuleFor(c => c.TripId, f => Guid.NewGuid())
                    .Generate();


                db.Add(customer);
                db.Add(driver);
                db.Add(review);
                await db.SaveChangesAsync();

                GetCustomerToDriverReviews.Request request = new GetCustomerToDriverReviews.Request
                {
                    CustomerId = customer.Id,
                    DriverId = driver.Id
                };

                GetCustomerToDriverReviews.Handler handler = new GetCustomerToDriverReviews.Handler(db);

                ICollection<GetCustomerToDriverReviews.Response> actualReviews = await handler.Handle(request).ConfigureAwait(false);

                foreach (GetCustomerToDriverReviews.Response actualReview in actualReviews)
                {
                    actualReview.Id.ShouldBe(review.Id);
                    actualReview.Rating.ShouldBe(review.Rating);
                    actualReview.TripId.ShouldBe(review.TripId);
                    actualReview.Created.ShouldBe(review.Created);
                    actualReview.Text.ShouldBe(review.Text);
                }
            }
        }
    }
}
