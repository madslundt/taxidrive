using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Shouldly;
using System;
using System.Collections.Generic;
using Source.Features.Review;

namespace UnitTests.Features.Review
{

    public class GetDriverReviewTests
    {
        private readonly IMediator _mediator;
        public GetDriverReviewTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            GetDriverReviews.Request request = new GetDriverReviews.Request
            {
                DriverId = Guid.Empty
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenDriverDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetDriverReviews.Request request = new GetDriverReviews.Request
                {
                    DriverId = Guid.NewGuid()
                };

                GetDriverReviews.Handler handler = new GetDriverReviews.Handler(db);

                ICollection<GetDriverReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitLessThan1()
        {
            GetDriverReviews.Request request = new GetDriverReviews.Request
            {
                DriverId = Guid.NewGuid(),
                Limit = 0
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitGreaterThan100()
        {
            GetDriverReviews.Request request = new GetDriverReviews.Request
            {
                DriverId = Guid.NewGuid(),
                Limit = 101
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithOffsetLessThan0()
        {
            GetDriverReviews.Request request = new GetDriverReviews.Request
            {
                DriverId = Guid.NewGuid(),
                Offset = -1
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenDriverHaveNoReviews()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(driver);
                await db.SaveChangesAsync();

                GetDriverReviews.Request request = new GetDriverReviews.Request
                {
                    DriverId = driver.Id
                };

                GetDriverReviews.Handler handler = new GetDriverReviews.Handler(db);

                ICollection<GetDriverReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }

        [Fact]
        public async Task ItShouldReturnReviewsWhenDriverHaveReviews()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.CustomerToDriverReviewContext.CustomerToDriverReview review = new Faker<DataModels.CustomerToDriverReviewContext.CustomerToDriverReview>()
                    .RuleFor(c => c.Driver, f => driver)
                    .RuleFor(c => c.CreatorId, f => Guid.NewGuid())
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .RuleFor(c => c.TripId, f => Guid.NewGuid())
                    .Generate();


                db.Add(driver);
                db.Add(review);
                await db.SaveChangesAsync();

                GetDriverReviews.Request request = new GetDriverReviews.Request
                {
                    DriverId = driver.Id
                };

                GetDriverReviews.Handler handler = new GetDriverReviews.Handler(db);

                ICollection<GetDriverReviews.Response> actualReviews = await handler.Handle(request).ConfigureAwait(false);

                foreach (GetDriverReviews.Response actualReview in actualReviews)
                {
                    actualReview.Id.ShouldBe(review.Id);
                    actualReview.CustomerId.ShouldBe(review.CreatorId);
                    actualReview.Rating.ShouldBe(review.Rating);
                    actualReview.TripId.ShouldBe(review.TripId);
                    actualReview.Created.ShouldBe(review.Created);
                    actualReview.Text.ShouldBe(review.Text);
                }
            }
        }
    }
}
