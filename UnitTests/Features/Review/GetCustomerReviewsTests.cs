using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Shouldly;
using System;
using System.Collections.Generic;
using Source.Features.Review;

namespace UnitTests.Features.Review
{

    public class GetCustomerReviewsTests
    {
        private readonly IMediator _mediator;
        public GetCustomerReviewsTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            GetCustomerReviews.Request request = new GetCustomerReviews.Request
            {
                CustomerId = Guid.Empty
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCustomerDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetCustomerReviews.Request request = new GetCustomerReviews.Request
                {
                    CustomerId = Guid.NewGuid()
                };

                GetCustomerReviews.Handler handler = new GetCustomerReviews.Handler(db);

                ICollection<GetCustomerReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitLessThan1()
        {
            GetCustomerReviews.Request request = new GetCustomerReviews.Request
            {
                CustomerId = Guid.NewGuid(),
                Limit = 0
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitGreaterThan100()
        {
            GetCustomerReviews.Request request = new GetCustomerReviews.Request
            {
                CustomerId = Guid.NewGuid(),
                Limit = 101
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithOffsetLessThan0()
        {
            GetCustomerReviews.Request request = new GetCustomerReviews.Request
            {
                CustomerId = Guid.NewGuid(),
                Offset = -1
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCustomerHaveNoReviews()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(customer);
                await db.SaveChangesAsync();

                GetCustomerReviews.Request request = new GetCustomerReviews.Request
                {
                    CustomerId = customer.Id
                };

                GetCustomerReviews.Handler handler = new GetCustomerReviews.Handler(db);

                ICollection<GetCustomerReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }

        [Fact]
        public async Task ItShouldReturnReviewsWhenCustomerHaveReviews()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                DataModels.DriverToCustomerReviewContext.DriverToCustomerReview review = new Faker<DataModels.DriverToCustomerReviewContext.DriverToCustomerReview>()
                    .RuleFor(c => c.Customer, f => customer)
                    .RuleFor(c => c.CreatorId, f => Guid.NewGuid())
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .RuleFor(c => c.TripId, f => Guid.NewGuid())
                    .Generate();


                db.Add(customer);
                db.Add(review);
                await db.SaveChangesAsync();

                GetCustomerReviews.Request request = new GetCustomerReviews.Request
                {
                    CustomerId = customer.Id
                };

                GetCustomerReviews.Handler handler = new GetCustomerReviews.Handler(db);

                ICollection<GetCustomerReviews.Response> actualReviews = await handler.Handle(request).ConfigureAwait(false);

                foreach (GetCustomerReviews.Response actualReview in actualReviews)
                {
                    actualReview.Id.ShouldBe(review.Id);
                    actualReview.DriverId.ShouldBe(review.CreatorId);
                    actualReview.Rating.ShouldBe(review.Rating);
                    actualReview.TripId.ShouldBe(review.TripId);
                    actualReview.Created.ShouldBe(review.Created);
                    actualReview.Text.ShouldBe(review.Text);
                }
            }
        }
    }
}
