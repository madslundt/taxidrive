using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using FluentValidation;
using Shouldly;
using Source.Features.Review;

namespace UnitTests.Features.Review
{

    public class AddDriverTripReviewTests
    {
        private readonly IMediator _mediator;
        public AddDriverTripReviewTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIdIsEmpty()
        {
            AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                .Generate();

            AddDriverTripReview.Request request = new Faker<AddDriverTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.Empty)
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                .Generate();

            AddDriverTripReview.Request request = new Faker<AddDriverTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.DriverId, f => Guid.Empty)
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenReviewIsNull()
        {
            AddDriverTripReview.Request request = new Faker<AddDriverTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => null)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRatingIsNull()
        {
            AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                .Generate();

            AddDriverTripReview.Request request = new Faker<AddDriverTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRatingIsLessThan1()
        {
            AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                .RuleFor(c => c.Rating, f => 0)
                .Generate();

            AddDriverTripReview.Request request = new Faker<AddDriverTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRatingIsGreaterThan5()
        {
            AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                .RuleFor(c => c.Rating, f => 6)
                .Generate();

            AddDriverTripReview.Request request = new Faker<AddDriverTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenTripDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(driver);
                await db.SaveChangesAsync();

                AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .Generate();

                AddDriverTripReview.Request request = new AddDriverTripReview.Request
                {
                    TripId = Guid.NewGuid(),
                    DriverId = driver.Id,
                    Review = review
                };

                AddDriverTripReview.Handler handler = new AddDriverTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenDriverDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(trip);
                await db.SaveChangesAsync();

                AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                    .RuleFor(c => c.Rating, f => 6)
                    .Generate();

                AddDriverTripReview.Request request = new AddDriverTripReview.Request
                {
                    TripId = trip.Id,
                    DriverId = Guid.NewGuid(),
                    Review = review
                };

                AddDriverTripReview.Handler handler = new AddDriverTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenTripAndDriverDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                    .RuleFor(c => c.Rating, f => 6)
                    .Generate();

                AddDriverTripReview.Request request = new AddDriverTripReview.Request
                {
                    TripId = Guid.NewGuid(),
                    DriverId = Guid.NewGuid(),
                    Review = review
                };

                AddDriverTripReview.Handler handler = new AddDriverTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task ItShouldAddReviewWithText()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.DriverId, f => driver.Id)
                    .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(driver);
                db.Add(trip);
                await db.SaveChangesAsync();

                AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .Generate();

                AddDriverTripReview.Request request = new AddDriverTripReview.Request
                {
                    TripId = trip.Id,
                    DriverId = driver.Id,
                    Review = review
                };

                AddDriverTripReview.Handler handler = new AddDriverTripReview.Handler(db);

                AddDriverTripReview.Response response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldNotBeNull();
                response.Id.ShouldNotBe(Guid.Empty);
            }
        }
        [Fact]
        public async Task ItShouldAddReviewWithoutText()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.DriverId, f => driver.Id)
                    .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(driver);
                db.Add(trip);
                await db.SaveChangesAsync();

                AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .Generate();

                AddDriverTripReview.Request request = new AddDriverTripReview.Request
                {
                    TripId = trip.Id,
                    DriverId = driver.Id,
                    Review = review
                };

                AddDriverTripReview.Handler handler = new AddDriverTripReview.Handler(db);

                AddDriverTripReview.Response response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldNotBeNull();
                response.Id.ShouldNotBe(Guid.Empty);
            }
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenDriverIsNotAssignedToTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(driver);
                db.Add(trip);
                await db.SaveChangesAsync();

                AddDriverTripReview.Review review = new Faker<AddDriverTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .Generate();

                AddDriverTripReview.Request request = new AddDriverTripReview.Request
                {
                    TripId = trip.Id,
                    DriverId = driver.Id,
                    Review = review
                };

                AddDriverTripReview.Handler handler = new AddDriverTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
    }
}
