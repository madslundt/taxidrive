using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using FluentValidation;
using Shouldly;
using Source.Features.Review;

namespace UnitTests.Features.Review
{

    public class AddCustomerTripReviewTests
    {
        private readonly IMediator _mediator;
        public AddCustomerTripReviewTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIdIsEmpty()
        {
            AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                .Generate();

            AddCustomerTripReview.Request request = new Faker<AddCustomerTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.Empty)
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                .Generate();

            AddCustomerTripReview.Request request = new Faker<AddCustomerTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.CustomerId, f => Guid.Empty)
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenReviewIsNull()
        {
            AddCustomerTripReview.Request request = new Faker<AddCustomerTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => null)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRatingIsNull()
        {
            AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                .Generate();

            AddCustomerTripReview.Request request = new Faker<AddCustomerTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRatingIsLessThan1()
        {
            AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                .RuleFor(c => c.Rating, f => 0)
                .Generate();

            AddCustomerTripReview.Request request = new Faker<AddCustomerTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRatingIsGreaterThan5()
        {
            AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                .RuleFor(c => c.Rating, f => 6)
                .Generate();

            AddCustomerTripReview.Request request = new Faker<AddCustomerTripReview.Request>()
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Review, f => review)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenTripDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(customer);
                await db.SaveChangesAsync();

                AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .Generate();

                AddCustomerTripReview.Request request = new AddCustomerTripReview.Request
                {
                    TripId = Guid.NewGuid(),
                    CustomerId = customer.Id,
                    Review = review
                };

                AddCustomerTripReview.Handler handler = new AddCustomerTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenCustomerDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(trip);
                await db.SaveChangesAsync();

                AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                    .RuleFor(c => c.Rating, f => 6)
                    .Generate();

                AddCustomerTripReview.Request request = new AddCustomerTripReview.Request
                {
                    TripId = trip.Id,
                    CustomerId = Guid.NewGuid(),
                    Review = review
                };

                AddCustomerTripReview.Handler handler = new AddCustomerTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenTripAndCustomerDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                    .RuleFor(c => c.Rating, f => 6)
                    .Generate();

                AddCustomerTripReview.Request request = new AddCustomerTripReview.Request
                {
                    TripId = Guid.NewGuid(),
                    CustomerId = Guid.NewGuid(),
                    Review = review
                };

                AddCustomerTripReview.Handler handler = new AddCustomerTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task ItShouldAddReviewWithText()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Customer, f => customer)
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                await db.SaveChangesAsync();

                AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .Generate();

                AddCustomerTripReview.Request request = new AddCustomerTripReview.Request
                {
                    TripId = trip.Id,
                    CustomerId = customer.Id,
                    Review = review
                };

                AddCustomerTripReview.Handler handler = new AddCustomerTripReview.Handler(db);

                AddCustomerTripReview.Response response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldNotBeNull();
                response.Id.ShouldNotBe(Guid.Empty);
            }
        }
        [Fact]
        public async Task ItShouldAddReviewWithoutText()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Customer, f => customer)
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                await db.SaveChangesAsync();

                AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .Generate();

                AddCustomerTripReview.Request request = new AddCustomerTripReview.Request
                {
                    TripId = trip.Id,
                    CustomerId = customer.Id,
                    Review = review
                };

                AddCustomerTripReview.Handler handler = new AddCustomerTripReview.Handler(db);

                AddCustomerTripReview.Response response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldNotBeNull();
                response.Id.ShouldNotBe(Guid.Empty);
            }
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenCustomerIsNotAssignedToTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                await db.SaveChangesAsync();

                AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .Generate();

                AddCustomerTripReview.Request request = new AddCustomerTripReview.Request
                {
                    TripId = trip.Id,
                    CustomerId = customer.Id,
                    Review = review
                };

                AddCustomerTripReview.Handler handler = new AddCustomerTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }

        // [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenTripHasNoDriver()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);

                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Customer, f => customer)
                    .RuleFor(c => c.OriginId, f => Guid.NewGuid())
                    .RuleFor(c => c.DestinationId, f => Guid.NewGuid())
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                await db.SaveChangesAsync();

                AddCustomerTripReview.Review review = new Faker<AddCustomerTripReview.Review>()
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .Generate();

                AddCustomerTripReview.Request request = new AddCustomerTripReview.Request
                {
                    TripId = trip.Id,
                    CustomerId = customer.Id,
                    Review = review
                };

                AddCustomerTripReview.Handler handler = new AddCustomerTripReview.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
    }
}
