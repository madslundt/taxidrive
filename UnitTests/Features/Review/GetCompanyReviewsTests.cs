using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Shouldly;
using System;
using System.Collections.Generic;
using Source.Features.Review;

namespace UnitTests.Features.Review
{

    public class GetCompanyReviewsTests
    {
        private readonly IMediator _mediator;
        public GetCompanyReviewsTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCompanyIdIsEmpty()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = Guid.Empty
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                    await _mediator.SendAsync(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCompanyDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = Guid.NewGuid()
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                ICollection<GetCompanyReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitLessThan1()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = Guid.NewGuid(),
                    Limit = 0
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                    await _mediator.SendAsync(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitGreaterThan100()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = Guid.NewGuid(),
                    Limit = 101
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                    await _mediator.SendAsync(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithOffsetLessThan0()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = Guid.NewGuid(),
                    Offset = -1
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                    await _mediator.SendAsync(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCompanyHaveNoDrivers()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

                db.Add(company);
                await db.SaveChangesAsync();

                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = company.Id
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                ICollection<GetCompanyReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCompanyHaveNoReviews()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.Company, f => company)
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .Generate();

                db.Add(company);
                db.Add(driver);
                await db.SaveChangesAsync();

                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = company.Id
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                ICollection<GetCompanyReviews.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldReturnReviewsWhenCompanyHaveReviews()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .RuleFor(c => c.Company, f => company)
                    .Generate();
                    // TODO generate list

                DataModels.CustomerToDriverReviewContext.CustomerToDriverReview review = new Faker<DataModels.CustomerToDriverReviewContext.CustomerToDriverReview>()
                    .RuleFor(c => c.Driver, f => driver)
                    .RuleFor(c => c.CreatorId, f => Guid.NewGuid())
                    .RuleFor(c => c.Rating, f => f.Random.Int(1, 5))
                    .RuleFor(c => c.Text, f => f.Lorem.Paragraph())
                    .RuleFor(c => c.TripId, f => Guid.NewGuid())
                    .Generate();

                db.Add(company);
                db.Add(driver);
                db.Add(review);
                await db.SaveChangesAsync();

                GetCompanyReviews.Request request = new GetCompanyReviews.Request
                {
                    CompanyId = company.Id
                };

                GetCompanyReviews.Handler handler = new GetCompanyReviews.Handler(db);

                ICollection<GetCompanyReviews.Response> actualReviews = await handler.Handle(request).ConfigureAwait(false);

                foreach (GetCompanyReviews.Response actualReview in actualReviews)
                {
                    actualReview.Id.ShouldBe(review.Id);
                    actualReview.CustomerId.ShouldBe(review.CreatorId);
                    actualReview.DriverId.ShouldBe(driver.Id);
                    actualReview.Rating.ShouldBe(review.Rating);
                    actualReview.TripId.ShouldBe(review.TripId);
                    actualReview.Created.ShouldBe(review.Created);
                    actualReview.Text.ShouldBe(review.Text);
                }
            }
        }
    }
}
