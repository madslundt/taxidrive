using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Company;
using Shouldly;
using System;

namespace UnitTests.Features.Company
{

    public class GetCompanyTests
    {
        private readonly IMediator _mediator;
        public GetCompanyTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowNullReferenceExceptionWhenRequestingWithNull()
        {
            GetCompany.Request request = null;

            await Assert.ThrowsAsync<NullReferenceException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithEmptyGuid()
        {
            GetCompany.Request request = new Faker<GetCompany.Request>()
                .RuleFor(c => c.CompanyId, f => Guid.Empty)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenCompanyDoesNotExist()
        {
            GetCompany.Request request = new Faker<GetCompany.Request>()
                .RuleFor(c => c.CompanyId, f => Guid.NewGuid())
                .Generate();

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnCompanyWhenExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

                db.Add(company);
                await db.SaveChangesAsync();

                GetCompany.Request request = new GetCompany.Request
                {
                    CompanyId = company.Id
                };

                GetCompany.Handler handler = new GetCompany.Handler(db);

                GetCompany.Response actualCompany = await handler.Handle(request).ConfigureAwait(false);

                actualCompany.Id.ShouldBe(company.Id);
                actualCompany.Name.ShouldBe(company.Name);
            }
        }
    }
}
