using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using FluentValidation;
using Shouldly;
using Source.Features.Trip;

namespace UnitTests.Features.Trip
{

    public class AddCustomerTripTests
    {
        private readonly IMediator _mediator;
        public AddCustomerTripTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.Empty,
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIsNull()
        {
            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = null
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOriginIsNull()
        {
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => null)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDestinationIsNull()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => null)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOriginAltitudeIsLessThan0()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Altitude, f => -1)
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOriginLatitudeIsLessThanMinus90()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => -91)
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOriginLatitudeIsGreaterThan90()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => 91)
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOriginLongitudeIsLessThanMinus180()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => -181)
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOriginLongitudeIsGreaterThan180()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => 181)
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDestinationAltitudeIsLessThan0()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => -1)
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDestinationLatitudeIsLessThanMinus180()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => -181)
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDestinationLatitudeIsGreaterThan180()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => 181)
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDestinationLongitudeIsLessThanMinus180()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => -181)
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDestinationLongitudeIsGreaterThan180()
        {
            AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .Generate();
            AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .RuleFor(c => c.Longitude, f => 181)
                .Generate();

            AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                .RuleFor(c => c.Origin, f => origin)
                .RuleFor(c => c.Destination, f => destination)
                .Generate();

            AddCustomerTrip.Request request = new AddCustomerTrip.Request
            {
                CustomerId = Guid.NewGuid(),
                Trip = trip
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldAddCustomerTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .Generate();

                AddCustomerTrip.Coordinate origin = new Faker<AddCustomerTrip.Coordinate>()
                    .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                    .Generate();
                AddCustomerTrip.Coordinate destination = new Faker<AddCustomerTrip.Coordinate>()
                    .RuleFor(c => c.Altitude, f => f.Random.Double(0, 60))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .RuleFor(c => c.Longitude, f => 181)
                    .Generate();

                AddCustomerTrip.Trip trip = new Faker<AddCustomerTrip.Trip>()
                    .RuleFor(c => c.Origin, f => origin)
                    .RuleFor(c => c.Destination, f => destination)
                    .Generate();

                db.Add(customer);
                await db.SaveChangesAsync();

                AddCustomerTrip.Request request = new AddCustomerTrip.Request
                {
                    CustomerId = Guid.NewGuid(),
                    Trip = trip
                };

                AddCustomerTrip.Handler handler = new AddCustomerTrip.Handler(db);

                AddCustomerTrip.Response actualCustomerTrip = await handler.Handle(request).ConfigureAwait(false);

                actualCustomerTrip.ShouldNotBeNull();
                actualCustomerTrip.Id.ShouldNotBe(Guid.Empty);
            }
        }
    }
}
