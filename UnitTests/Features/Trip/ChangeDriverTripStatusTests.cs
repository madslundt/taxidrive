using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using Shouldly;
using FluentValidation;
using Source.Features.Trip;
using System.Collections.Generic;
using DataModels.TripPositionContext;

namespace UnitTests.Features.Trip
{

    public class ChangeDriverTripStatusTests
    {
        private readonly IMediator _mediator;
        public ChangeDriverTripStatusTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                .RuleFor(c => c.DriverId, f => Guid.Empty)
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => f.PickRandom<TripStatus>())
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIdIsEmpty()
        {
            ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.Empty)
                .RuleFor(c => c.Status, f => TripStatus.DriverCancelled)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripStatusIsCustomerCancelled()
        {
            ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => TripStatus.CustomerCancelled)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripStatusIsAccepted()
        {
            ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => TripStatus.Accepted)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripStatusIsRequesting()
        {
            ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => TripStatus.Requesting)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenTripDoesNotExist()
        {
            ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => TripStatus.DriverCancelled)
                .Generate();

            await Assert.ThrowsAsync<MemberAccessException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenDriverDoesNotHaveAccessToTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Requesting)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                    .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                    .RuleFor(c => c.TripId, f => trip.Id)
                    .RuleFor(c => c.Status, f => TripStatus.DriverCancelled)
                    .Generate();

                ChangeDriverTripStatus.Handler handler = new ChangeDriverTripStatus.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldChangeDriverTripStatus()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.DriverContext.Driver customer = new Faker<DataModels.DriverContext.Driver>()
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Driver, f => customer)
                    .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Requesting)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                ChangeDriverTripStatus.Request request = new Faker<ChangeDriverTripStatus.Request>()
                    .RuleFor(c => c.DriverId, f => customer.Id)
                    .RuleFor(c => c.TripId, f => trip.Id)
                    .RuleFor(c => c.Status, f => TripStatus.DriverCancelled)
                    .Generate();

                ChangeDriverTripStatus.Handler handler = new ChangeDriverTripStatus.Handler(db);

                ChangeDriverTripStatus.Response actualResponse = await handler.Handle(request).ConfigureAwait(false);

                actualResponse.ShouldNotBeNull();
                actualResponse.Id.ShouldNotBe(Guid.Empty);
            }
        }
    }
}
