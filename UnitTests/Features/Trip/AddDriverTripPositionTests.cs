using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using FluentValidation;
using Shouldly;
using Source.Features.Trip;
using System.Collections.Generic;

namespace UnitTests.Features.Trip
{

    public class AddDriverTripPositionTests
    {
        private readonly IMediator _mediator;
        public AddDriverTripPositionTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => f.Random.Double(0, 100))
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.Empty,
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIdIsEmpty()
        {
            AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => f.Random.Double(0, 100))
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.Empty,
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionIsNull()
        {
            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = null
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionSpeedIsLessThan0()
        {
            AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLatitudeIsLessThanMinus90()
        {
            AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => -90)
                .Generate();

            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLatitudeIsGreaterThan90()
        {
            AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => 91)
                .Generate();

            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLongitudeIsLessThanMinus180()
        {
            AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => -181)
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLongitudeIsGreaterThan180()
        {
            AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => 181)
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenTripStatusIsDifferentFromDriving()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                var statuses = new List<DataModels.TripPositionContext.TripStatus>();
                foreach (DataModels.TripPositionContext.TripStatus status in Enum.GetValues(typeof(DataModels.TripPositionContext.TripStatus)))
                {
                    if (status != DataModels.TripPositionContext.TripStatus.Driving)
                    {
                        statuses.Add(status);
                    }
                }

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .Generate();

                AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                    .RuleFor(c => c.Accuracy, f => f.Random.Double())
                    .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                    .RuleFor(c => c.Heading, f => f.Random.Double())
                    .RuleFor(c => c.Speed, f => -1)
                    .RuleFor(c => c.Altitude, f => f.Random.Double())
                    .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                        .RuleFor(c => c.Driver, f => driver)
                        .Generate();

                    DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                        .RuleFor(c => c.Status, f => f.PickRandom(statuses))
                        .RuleFor(c => c.Trip, f => trip)
                        .Generate();

                db.Add(driver);
                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
                {
                    DriverId = driver.Id,
                    TripId = trip.Id,
                    Position = position
                };


                AddDriverTripPosition.Handler handler = new AddDriverTripPosition.Handler(db);

                await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenDriverIdIsNotAssignedToTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                var statuses = new List<DataModels.TripPositionContext.TripStatus>();
                foreach (DataModels.TripPositionContext.TripStatus status in Enum.GetValues(typeof(DataModels.TripPositionContext.TripStatus)))
                {
                    if (status != DataModels.TripPositionContext.TripStatus.Driving)
                    {
                        statuses.Add(status);
                    }
                }

                AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                    .RuleFor(c => c.Accuracy, f => f.Random.Double())
                    .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                    .RuleFor(c => c.Heading, f => f.Random.Double())
                    .RuleFor(c => c.Speed, f => -1)
                    .RuleFor(c => c.Altitude, f => f.Random.Double())
                    .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                        .Generate();

                    DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                        .RuleFor(c => c.Status, f => f.PickRandom(statuses))
                        .RuleFor(c => c.Trip, f => trip)
                        .Generate();

                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
                {
                    DriverId = Guid.NewGuid(),
                    TripId = trip.Id,
                    Position = position
                };


                AddDriverTripPosition.Handler handler = new AddDriverTripPosition.Handler(db);

                await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldAddDriverTripPosition()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .Generate();

                AddDriverTripPosition.Position position = new Faker<AddDriverTripPosition.Position>()
                    .RuleFor(c => c.Accuracy, f => f.Random.Double())
                    .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                    .RuleFor(c => c.Heading, f => f.Random.Double())
                    .RuleFor(c => c.Speed, f => -1)
                    .RuleFor(c => c.Altitude, f => f.Random.Double())
                    .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                        .RuleFor(c => c.Driver, f => driver)
                        .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Driving)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(driver);
                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddDriverTripPosition.Request request = new AddDriverTripPosition.Request
                {
                    DriverId = driver.Id,
                    TripId = trip.Id,
                    Position = position
                };

                AddDriverTripPosition.Handler handler = new AddDriverTripPosition.Handler(db);

                AddDriverTripPosition.Response actualTripPostion = await handler.Handle(request).ConfigureAwait(false);

                actualTripPostion.ShouldNotBeNull();
                actualTripPostion.Id.ShouldNotBe(Guid.Empty);
            }
        }
    }
}
