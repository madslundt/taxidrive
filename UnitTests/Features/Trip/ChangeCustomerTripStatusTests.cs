using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using Shouldly;
using FluentValidation;
using Source.Features.Trip;
using System.Collections.Generic;
using DataModels.TripPositionContext;

namespace UnitTests.Features.Trip
{

    public class ChangeCustomerTripStatusTests
    {
        private readonly IMediator _mediator;
        public ChangeCustomerTripStatusTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            ChangeCustomerTripStatus.Request request = new Faker<ChangeCustomerTripStatus.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.Empty)
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => f.PickRandom<TripStatus>())
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIdIsEmpty()
        {
            ChangeCustomerTripStatus.Request request = new Faker<ChangeCustomerTripStatus.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.Empty)
                .RuleFor(c => c.Status, f => TripStatus.CustomerCancelled)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripStatusIsDifferentFromCustomerCancelled()
        {
            var statuses = new List<DataModels.TripPositionContext.TripStatus>();
            foreach (DataModels.TripPositionContext.TripStatus status in Enum.GetValues(typeof(DataModels.TripPositionContext.TripStatus)))
            {
                if (status != DataModels.TripPositionContext.TripStatus.CustomerCancelled)
                {
                    statuses.Add(status);
                }
            }

            ChangeCustomerTripStatus.Request request = new Faker<ChangeCustomerTripStatus.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => f.PickRandom(statuses))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenTripDoesNotExist()
        {
            ChangeCustomerTripStatus.Request request = new Faker<ChangeCustomerTripStatus.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.TripId, f => Guid.NewGuid())
                .RuleFor(c => c.Status, f => TripStatus.CustomerCancelled)
                .Generate();

            await Assert.ThrowsAsync<MemberAccessException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowMemberAccessExceptionWhenCustomerDoesNotHaveAccessToTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Requesting)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                ChangeCustomerTripStatus.Request request = new Faker<ChangeCustomerTripStatus.Request>()
                    .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                    .RuleFor(c => c.TripId, f => trip.Id)
                    .RuleFor(c => c.Status, f => TripStatus.CustomerCancelled)
                    .Generate();

                ChangeCustomerTripStatus.Handler handler = new ChangeCustomerTripStatus.Handler(db);

                await Assert.ThrowsAsync<MemberAccessException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldChangeCustomerTripStatus()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .Generate();
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Customer, f => customer)
                    .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Requesting)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                ChangeCustomerTripStatus.Request request = new Faker<ChangeCustomerTripStatus.Request>()
                    .RuleFor(c => c.CustomerId, f => customer.Id)
                    .RuleFor(c => c.TripId, f => trip.Id)
                    .RuleFor(c => c.Status, f => TripStatus.CustomerCancelled)
                    .Generate();

                ChangeCustomerTripStatus.Handler handler = new ChangeCustomerTripStatus.Handler(db);

                ChangeCustomerTripStatus.Response actualResponse = await handler.Handle(request).ConfigureAwait(false);

                actualResponse.ShouldNotBeNull();
                actualResponse.Id.ShouldNotBe(Guid.Empty);
            }
        }
    }
}
