using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using FluentValidation;
using Shouldly;
using Source.Features.Trip;
using System.Collections.Generic;

namespace UnitTests.Features.Trip
{

    public class AddCustomerTripPositionTests
    {
        private readonly IMediator _mediator;
        public AddCustomerTripPositionTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => f.Random.Double(0, 100))
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.Empty,
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIdIsEmpty()
        {
            AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => f.Random.Double(0, 100))
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.NewGuid(),
                TripId = Guid.Empty,
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionIsNull()
        {
            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = null
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionSpeedIsLessThan0()
        {
            AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLatitudeIsLessThanMinus90()
        {
            AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => -90)
                .Generate();

            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLatitudeIsGreaterThan90()
        {
            AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                .RuleFor(c => c.Latitude, f => 91)
                .Generate();

            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLongitudeIsLessThanMinus180()
        {
            AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => -181)
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenPositionLongitudeIsGreaterThan180()
        {
            AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                .RuleFor(c => c.Accuracy, f => f.Random.Double())
                .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                .RuleFor(c => c.Heading, f => f.Random.Double())
                .RuleFor(c => c.Speed, f => -1)
                .RuleFor(c => c.Altitude, f => f.Random.Double())
                .RuleFor(c => c.Longitude, f => 181)
                .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                .Generate();

            AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
            {
                CustomerId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                Position = position
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenTripStatusIsDifferentFromDriving()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                var statuses = new List<DataModels.TripPositionContext.TripStatus>();
                foreach (DataModels.TripPositionContext.TripStatus status in Enum.GetValues(typeof(DataModels.TripPositionContext.TripStatus)))
                {
                    if (status != DataModels.TripPositionContext.TripStatus.Driving)
                    {
                        statuses.Add(status);
                    }
                }

                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .Generate();

                AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                    .RuleFor(c => c.Accuracy, f => f.Random.Double())
                    .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                    .RuleFor(c => c.Heading, f => f.Random.Double())
                    .RuleFor(c => c.Speed, f => -1)
                    .RuleFor(c => c.Altitude, f => f.Random.Double())
                    .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                        .RuleFor(c => c.Customer, f => customer)
                        .Generate();

                    DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                        .RuleFor(c => c.Status, f => f.PickRandom(statuses))
                        .RuleFor(c => c.Trip, f => trip)
                        .Generate();

                db.Add(customer);
                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
                {
                    CustomerId = customer.Id,
                    TripId = trip.Id,
                    Position = position
                };


                AddCustomerTripPosition.Handler handler = new AddCustomerTripPosition.Handler(db);

                await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenCustomerIdIsNotAssignedToTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                var statuses = new List<DataModels.TripPositionContext.TripStatus>();
                foreach (DataModels.TripPositionContext.TripStatus status in Enum.GetValues(typeof(DataModels.TripPositionContext.TripStatus)))
                {
                    if (status != DataModels.TripPositionContext.TripStatus.Driving)
                    {
                        statuses.Add(status);
                    }
                }

                AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                    .RuleFor(c => c.Accuracy, f => f.Random.Double())
                    .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                    .RuleFor(c => c.Heading, f => f.Random.Double())
                    .RuleFor(c => c.Speed, f => -1)
                    .RuleFor(c => c.Altitude, f => f.Random.Double())
                    .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                        .Generate();

                    DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                        .RuleFor(c => c.Status, f => f.PickRandom(statuses))
                        .RuleFor(c => c.Trip, f => trip)
                        .Generate();

                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
                {
                    CustomerId = Guid.NewGuid(),
                    TripId = trip.Id,
                    Position = position
                };


                AddCustomerTripPosition.Handler handler = new AddCustomerTripPosition.Handler(db);

                await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldAddCustomerTripPosition()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .Generate();

                AddCustomerTripPosition.Position position = new Faker<AddCustomerTripPosition.Position>()
                    .RuleFor(c => c.Accuracy, f => f.Random.Double())
                    .RuleFor(c => c.AltitudeAccuracy, f => f.Random.Double())
                    .RuleFor(c => c.Heading, f => f.Random.Double())
                    .RuleFor(c => c.Speed, f => -1)
                    .RuleFor(c => c.Altitude, f => f.Random.Double())
                    .RuleFor(c => c.Longitude, f => f.Random.Double(-180, 180))
                    .RuleFor(c => c.Latitude, f => f.Random.Double(-90, 90))
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                        .RuleFor(c => c.Customer, f => customer)
                        .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Driving)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddCustomerTripPosition.Request request = new AddCustomerTripPosition.Request
                {
                    CustomerId = customer.Id,
                    TripId = trip.Id,
                    Position = position
                };

                AddCustomerTripPosition.Handler handler = new AddCustomerTripPosition.Handler(db);

                AddCustomerTripPosition.Response actualTripPostion = await handler.Handle(request).ConfigureAwait(false);

                actualTripPostion.ShouldNotBeNull();
                actualTripPostion.Id.ShouldNotBe(Guid.Empty);
            }
        }
    }
}
