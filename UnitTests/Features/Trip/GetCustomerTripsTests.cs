using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using Shouldly;
using FluentValidation;
using Source.Features.Trip;
using System.Collections.Generic;
using DataModels.TripPositionContext;

namespace UnitTests.Features.Trip
{

    public class GetCustomerTripsTests
    {
        private readonly IMediator _mediator;
        public GetCustomerTripsTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            GetCustomerTrips.Request request = new Faker<GetCustomerTrips.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.Empty)
                .RuleFor(c => c.Limit, f => f.Random.Int(1, 100))
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenLimitIsLessThan1()
        {
            GetCustomerTrips.Request request = new Faker<GetCustomerTrips.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => 0)
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenLimitIsGreaterThan100()
        {
            GetCustomerTrips.Request request = new Faker<GetCustomerTrips.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => 101)
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOffsetIsLessThan0()
        {
            GetCustomerTrips.Request request = new Faker<GetCustomerTrips.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => f.Random.Int(1, 100))
                .RuleFor(c => c.Offset, f => -1)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCustomerDoesNotExist()
        {
            GetCustomerTrips.Request request = new Faker<GetCustomerTrips.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .Generate();

            var actualResponse = await _mediator.SendAsync(request).ConfigureAwait(false);

            actualResponse.ShouldBeEmpty();
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCustomerHaveNoTrips()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .Generate();

                db.Add(customer);
                await db.SaveChangesAsync();

                GetCustomerTrips.Request request = new Faker<GetCustomerTrips.Request>()
                    .RuleFor(c => c.CustomerId, f => customer.Id)
                    .Generate();

                GetCustomerTrips.Handler handler = new GetCustomerTrips.Handler(db);

                var actualResponse = await handler.Handle(request).ConfigureAwait(false);

                actualResponse.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldReturnCustomerTrips()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Customer, f => customer)
                    .Generate();

                db.Add(customer);
                db.Add(trip);
                await db.SaveChangesAsync();

                GetCustomerTrips.Request request = new Faker<GetCustomerTrips.Request>()
                    .RuleFor(c => c.CustomerId, f => customer.Id)
                    .Generate();

                GetCustomerTrips.Handler handler = new GetCustomerTrips.Handler(db);

                var actualResponse = await handler.Handle(request).ConfigureAwait(false);

                actualResponse.Count.ShouldBe(1);
            }
        }
    }
}
