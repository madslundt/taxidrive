using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using Shouldly;
using FluentValidation;
using Source.Features.Trip;
using System.Collections.Generic;

namespace UnitTests.Features.Trip
{

    public class AddDriverTripTests
    {
        private readonly IMediator _mediator;
        public AddDriverTripTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            AddDriverTrip.Request request = new AddDriverTrip.Request
            {
                DriverId = Guid.Empty,
                TripId = Guid.NewGuid()
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenTripIdIsEmpty()
        {
            AddDriverTrip.Request request = new AddDriverTrip.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.Empty
            };

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenTripIdDoesNotExist()
        {
            AddDriverTrip.Request request = new AddDriverTrip.Request
            {
                DriverId = Guid.NewGuid(),
                TripId = Guid.NewGuid()
            };

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenDriverIdIsNotAssignedForTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .Generate();

                db.Add(trip);
                await db.SaveChangesAsync();

                AddDriverTrip.Request request = new AddDriverTrip.Request
                {
                    DriverId = Guid.NewGuid(),
                    TripId = trip.Id
                };

                AddDriverTrip.Handler handler = new AddDriverTrip.Handler(db);

                await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenTripHasDifferentStatusThanRequesting()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                var statuses = new List<DataModels.TripPositionContext.TripStatus>();
                foreach (DataModels.TripPositionContext.TripStatus status in Enum.GetValues(typeof(DataModels.TripPositionContext.TripStatus)))
                {
                    if (status != DataModels.TripPositionContext.TripStatus.Requesting)
                    {
                        statuses.Add(status);
                    }
                }

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => f.PickRandom(statuses))
                    .RuleFor(c => c.TripId, f => trip.Id)
                    .Generate();

                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddDriverTrip.Request request = new AddDriverTrip.Request
                {
                    DriverId = Guid.NewGuid(),
                    TripId = trip.Id
                };

                AddDriverTrip.Handler handler = new AddDriverTrip.Handler(db);

                await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenADriverIsAlreadyAssigned()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Driver, f => driver)
                    .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Requesting)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(driver);
                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddDriverTrip.Request request = new AddDriverTrip.Request
                {
                    DriverId = driver.Id,
                    TripId = trip.Id
                };

                AddDriverTrip.Handler handler = new AddDriverTrip.Handler(db);

                await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                    await handler.Handle(request).ConfigureAwait(false)
                ).ConfigureAwait(false);
            }
        }
        // [Fact]
        public async Task ItShouldAddDriverToTrip()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .Generate();

                DataModels.TripPositionContext.TripPosition tripPosition = new Faker<DataModels.TripPositionContext.TripPosition>()
                    .RuleFor(c => c.Status, f => DataModels.TripPositionContext.TripStatus.Requesting)
                    .RuleFor(c => c.Trip, f => trip)
                    .Generate();

                db.Add(trip);
                db.Add(tripPosition);
                await db.SaveChangesAsync();

                AddDriverTrip.Request request = new AddDriverTrip.Request
                {
                    DriverId = Guid.NewGuid(),
                    TripId = trip.Id
                };

                AddDriverTrip.Handler handler = new AddDriverTrip.Handler(db);

                AddDriverTrip.Response actualDriverTrip = await handler.Handle(request).ConfigureAwait(false);

                actualDriverTrip.ShouldNotBeNull();
                actualDriverTrip.Id.ShouldNotBe(Guid.Empty);
            }
        }
    }
}
