using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using System;
using Shouldly;
using FluentValidation;
using Source.Features.Trip;
using System.Collections.Generic;
using DataModels.TripPositionContext;

namespace UnitTests.Features.Trip
{

    public class GetDriverTripsTests
    {
        private readonly IMediator _mediator;
        public GetDriverTripsTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenDriverIdIsEmpty()
        {
            GetDriverTrips.Request request = new Faker<GetDriverTrips.Request>()
                .RuleFor(c => c.DriverId, f => Guid.Empty)
                .RuleFor(c => c.Limit, f => f.Random.Int(1, 100))
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenLimitIsLessThan1()
        {
            GetDriverTrips.Request request = new Faker<GetDriverTrips.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => 0)
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenLimitIsGreaterThan100()
        {
            GetDriverTrips.Request request = new Faker<GetDriverTrips.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => 101)
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenOffsetIsLessThan0()
        {
            GetDriverTrips.Request request = new Faker<GetDriverTrips.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => f.Random.Int(1, 100))
                .RuleFor(c => c.Offset, f => -1)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenDriverDoesNotExist()
        {
            GetDriverTrips.Request request = new Faker<GetDriverTrips.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .Generate();

            var actualResponse = await _mediator.SendAsync(request).ConfigureAwait(false);

            actualResponse.ShouldBeEmpty();
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenDriverHaveNoTrips()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .Generate();

                db.Add(driver);
                await db.SaveChangesAsync();

                GetDriverTrips.Request request = new Faker<GetDriverTrips.Request>()
                    .RuleFor(c => c.DriverId, f => driver.Id)
                    .Generate();

                GetDriverTrips.Handler handler = new GetDriverTrips.Handler(db);

                var actualResponse = await handler.Handle(request).ConfigureAwait(false);

                actualResponse.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldReturnDriverTrips()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .Generate();

                DataModels.TripContext.Trip trip = new Faker<DataModels.TripContext.Trip>()
                    .RuleFor(c => c.Driver, f => driver)
                    .Generate();

                db.Add(driver);
                db.Add(trip);
                await db.SaveChangesAsync();

                GetDriverTrips.Request request = new Faker<GetDriverTrips.Request>()
                    .RuleFor(c => c.DriverId, f => driver.Id)
                    .Generate();

                GetDriverTrips.Handler handler = new GetDriverTrips.Handler(db);

                var actualResponse = await handler.Handle(request).ConfigureAwait(false);

                actualResponse.Count.ShouldBe(1);
            }
        }
    }
}
