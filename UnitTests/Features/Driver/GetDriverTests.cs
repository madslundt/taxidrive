using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Driver;
using Shouldly;
using System;

namespace UnitTests.Features.Driver
{

    public class GetDriverTests
    {
        private readonly IMediator _mediator;
        public GetDriverTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowNullReferenceExceptionWhenRequestingWithNull()
        {
            GetDriver.Request request = null;

            await Assert.ThrowsAsync<NullReferenceException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithEmptyGuid()
        {
            GetDriver.Request request = new Faker<GetDriver.Request>()
                .RuleFor(c => c.DriverId, f => Guid.Empty)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenDriverDoesNotExist()
        {
            Guid driverId = Guid.NewGuid();
            GetDriver.Request request = new Faker<GetDriver.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .Generate();

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnDriverWhenExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .RuleFor(c => c.Company, f => company)
                    .Generate();

                db.Add(company);
                db.Add(driver);
                await db.SaveChangesAsync();

                GetDriver.Request request = new GetDriver.Request
                {
                    DriverId = driver.Id
                };

                GetDriver.Handler handler = new GetDriver.Handler(db);

                GetDriver.Response actualDriver = await handler.Handle(request).ConfigureAwait(false);

                actualDriver.Id.ShouldBe(driver.Id);
                actualDriver.FirstName.ShouldBe(driver.FirstName);
                actualDriver.LastName.ShouldBe(driver.LastName);
                actualDriver.Email.ShouldBe(driver.Email);
                actualDriver.Company.Id.ShouldBe(company.Id);
            }
        }
    }
}
