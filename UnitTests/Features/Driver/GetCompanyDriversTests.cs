using Bogus;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Driver;
using Shouldly;
using System;
using System.Collections.Generic;

namespace UnitTests.Features.Driver
{

    public class GetCompanyDriverTests
    {
        private readonly IMediator _mediator;
        public GetCompanyDriverTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCompanyIdIsEmpty()
        {
            GetCompanyDrivers.Request request = new Faker<GetCompanyDrivers.Request>()
                    .RuleFor(c => c.CompanyId, f => Guid.Empty)
                    .RuleFor(c => c.Limit, f => f.Random.Int(1, 100))
                    .RuleFor(c => c.Offset, f => f.Random.Int(0))
                    .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCompanyDoesNotExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                GetCompanyDrivers.Request request = new Faker<GetCompanyDrivers.Request>()
                    .RuleFor(c => c.CompanyId, f => Guid.NewGuid())
                    .RuleFor(c => c.Limit, f => f.Random.Int(1, 100))
                    .RuleFor(c => c.Offset, f => f.Random.Int(0))
                    .Generate();

                GetCompanyDrivers.Handler handler = new GetCompanyDrivers.Handler(db);

                ICollection<GetCompanyDrivers.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitLessThan1()
        {
            GetCompanyDrivers.Request request = new Faker<GetCompanyDrivers.Request>()
                .RuleFor(c => c.CompanyId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => f.Random.Int(Int32.MinValue, 0))
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithLimitGreaterThan100()
        {
            GetCompanyDrivers.Request request = new Faker<GetCompanyDrivers.Request>()
                .RuleFor(c => c.CompanyId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => f.Random.Int(101))
                .RuleFor(c => c.Offset, f => f.Random.Int(0))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenRequestingWithOffsetLessThan0()
        {
            GetCompanyDrivers.Request request = new Faker<GetCompanyDrivers.Request>()
                .RuleFor(c => c.CompanyId, f => Guid.NewGuid())
                .RuleFor(c => c.Limit, f => f.Random.Int(1, 100))
                .RuleFor(c => c.Offset, f => f.Random.Int(Int16.MinValue, -1))
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnEmptyListWhenCompanyHasNoDrivers()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

                db.Add(company);
                await db.SaveChangesAsync();

                GetCompanyDrivers.Request request = new GetCompanyDrivers.Request
                {
                    CompanyId = company.Id
                };

                GetCompanyDrivers.Handler handler = new GetCompanyDrivers.Handler(db);

                ICollection<GetCompanyDrivers.Response> response = await handler.Handle(request).ConfigureAwait(false);

                response.ShouldBeEmpty();
            }
        }
        [Fact]
        public async Task ItShouldReturnCompanyDriversWhenExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

                DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                    .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                    .RuleFor(c => c.LastName, f => f.Person.LastName)
                    .RuleFor(c => c.Email, f => f.Person.Email)
                    .RuleFor(c => c.Company, f => company)
                    .Generate();
                    // TODO generate list

                db.Add(company);
                db.Add(driver);
                await db.SaveChangesAsync();

                GetCompanyDrivers.Request request = new GetCompanyDrivers.Request
                {
                    CompanyId = company.Id
                };

                GetCompanyDrivers.Handler handler = new GetCompanyDrivers.Handler(db);

                ICollection<GetCompanyDrivers.Response> actualDrivers = await handler.Handle(request).ConfigureAwait(false);

                foreach (GetCompanyDrivers.Response actualDriver in actualDrivers)
                {
                    actualDriver.Id.ShouldBe(driver.Id);
                    actualDriver.FirstName.ShouldBe(driver.FirstName);
                    actualDriver.LastName.ShouldBe(driver.LastName);
                    actualDriver.Email.ShouldBe(driver.Email);
                }
            }
        }
    }
}
