using Bogus;
using FluentValidation;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Customer;
using Shouldly;
using System;

namespace UnitTests.Features.Customer
{

    public class GetCustomerTests
    {
        private readonly IMediator _mediator;
        public GetCustomerTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationContext>(options => options.UseInMemoryDatabase());

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowNullReferenceExceptionWhenRequestingWithNull()
        {
            GetCustomer.Request request = null;

            await Assert.ThrowsAsync<NullReferenceException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowValidationExceptionWhenCustomerIdIsEmpty()
        {
            GetCustomer.Request request = new Faker<GetCustomer.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.Empty)
                .Generate();

            await Assert.ThrowsAsync<FluentValidation.ValidationException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenCustomerDoesNotExist()
        {
            GetCustomer.Request request = new Faker<GetCustomer.Request>()
                .RuleFor(c => c.CustomerId, f => Guid.NewGuid())
                .Generate();

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnCustomerWhenExist()
        {
            using (var db = DbContext.InMemoryApplicationContext())
            {
                Randomizer.Seed = new Random(3897234);
                DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                    .RuleFor(c => c.FirstName, f => Guid.NewGuid().ToString())
                    .RuleFor(c => c.LastName, f => Guid.NewGuid().ToString())
                    .RuleFor(c => c.Email, f => Guid.NewGuid().ToString())
                    .Generate();

                db.Add(customer);
                await db.SaveChangesAsync();

                GetCustomer.Request request = new GetCustomer.Request
                {
                    CustomerId = customer.Id
                };

                GetCustomer.Handler handler = new GetCustomer.Handler(db);

                GetCustomer.Response actualCustomer = await handler.Handle(request).ConfigureAwait(false);

                actualCustomer.Id.ShouldBe(customer.Id);
                actualCustomer.FirstName.ShouldBe(customer.FirstName);
                actualCustomer.LastName.ShouldBe(customer.LastName);
                actualCustomer.Email.ShouldBe(customer.Email);
            }
        }
    }
}
