using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataModels.DriverToCustomerReviewContext
{
    public class DriverToCustomerReviewConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<DriverToCustomerReview>(b =>
            {
                b.Property(review => review.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.HasOne(r => r.Customer)
                    .WithMany(r => r.DriverToCustomerReviews)
                    .HasForeignKey(fk => fk.CustomerId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Trip)
                    .WithOne(r => r.DriverToCustomerReview)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Creator)
                    .WithMany(r => r.DriverToCustomerReviews)
                    .HasForeignKey(fk => fk.CreatorId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasAlternateKey(k => new {k.TripId, k.CustomerId, k.CreatorId});

                b.HasKey(k => k.Id);
                b.ToTable("DriverToCustomerReviews");
            });

        }
    }
}
