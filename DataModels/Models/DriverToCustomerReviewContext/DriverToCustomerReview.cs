using System;
using DataModels.CustomerContext;
using DataModels.DriverContext;
using DataModels.TripContext;

namespace DataModels.DriverToCustomerReviewContext
{
    public class DriverToCustomerReview
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid TripId { get; set; }
        public Trip Trip { get; set; }

        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }

        public Guid CreatorId { get; set; }
        public Driver Creator { get; set; }

        public int Rating { get; set; }
        public string Text { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
    }
}
