using Microsoft.EntityFrameworkCore;

namespace DataModels.DriverContext
{
    public class DriverConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<Driver>(b =>
            {
                b.Property(driver => driver.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.Property(driver => driver.FirstName)
                    .HasMaxLength(160)
                    .IsRequired();
                b.Property(driver => driver.LastName)
                    .HasMaxLength(160)
                    .IsRequired();

                b.Property(driver => driver.Email)
                    .HasMaxLength(320)
                    .IsRequired();

                b.HasOne(r => r.Company)
                    .WithMany(r => r.Drivers)
                    .HasForeignKey(fk => fk.CompanyId);

                b.HasIndex(i => i.Email)
                    .IsUnique();

                b.HasKey(k => k.Id);
                b.ToTable("Drivers");
            });

        }
    }
}
