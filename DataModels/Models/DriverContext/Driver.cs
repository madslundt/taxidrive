using System;
using System.Collections.Generic;
using DataModels.CustomerToDriverReviewContext;
using DataModels.DriverToCustomerReviewContext;
using DataModels.DriverTripPositionContext;
using DataModels.TripContext;
using DataModels.CompanyContext;

namespace DataModels.DriverContext
{
    public class Driver
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public Guid CompanyId { get; set; }
        public Company Company { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;

        public ICollection<Trip> Trips { get; set; }

        public ICollection<DriverToCustomerReview> DriverToCustomerReviews { get; set; }
        public ICollection<CustomerToDriverReview> CustomerToDriverReviews { get; set; }
        public ICollection<DriverTripPosition> DriverTripPositions { get; set; }
    }
}
