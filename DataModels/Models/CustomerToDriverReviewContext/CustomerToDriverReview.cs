using System;
using DataModels.CustomerContext;
using DataModels.DriverContext;
using DataModels.TripContext;

namespace DataModels.CustomerToDriverReviewContext
{
    public class CustomerToDriverReview
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid TripId { get; set; }
        public Trip Trip { get; set; }

        public Guid CreatorId { get; set; }
        public Customer Creator { get; set; }

        public Guid DriverId { get; set; }
        public Driver Driver { get; set; }

        public int Rating { get; set; }
        public string Text { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
    }
}
