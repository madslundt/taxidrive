using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataModels.CustomerToDriverReviewContext
{
    public class CustomerToDriverReviewConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<CustomerToDriverReview>(b =>
            {
                b.Property(review => review.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.HasOne(r => r.Driver)
                    .WithMany(r => r.CustomerToDriverReviews)
                    .HasForeignKey(fk => fk.DriverId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Trip)
                    .WithOne(r => r.CustomerToDriverReview)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Driver)
                    .WithMany(r => r.CustomerToDriverReviews)
                    .HasForeignKey(fk => fk.DriverId)
                    .OnDelete(DeleteBehavior.Restrict);


                b.HasAlternateKey(k => new {k.TripId, k.CreatorId, k.DriverId});

                b.HasKey(k => k.Id);
                b.ToTable("CustomerToDriverReviews");
            });

        }
    }
}
