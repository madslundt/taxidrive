using System;
using DataModels.PositionContext;
using DataModels.TripPositionContext;
using DataModels.DriverContext;

namespace DataModels.DriverTripPositionContext
{
    public class DriverTripPosition
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid TripPositionId { get; set; }
        public TripPosition TripPosition { get; set; }
        public Guid PositionId { get; set; }
        public Position Position { get; set; }
        public Guid DriverId { get; set; }
        public Driver Driver { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
    }
}
