using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataModels.DriverTripPositionContext
{
    public class DriverTripPositionConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<DriverTripPosition>(b =>
            {
                b.Property(position => position.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.HasOne(r => r.Position)
                    .WithMany(r => r.DriverTripPositions)
                    .HasForeignKey(fk => fk.PositionId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.TripPosition)
                    .WithMany(r => r.DriverTripPositions)
                    .HasForeignKey(fk => fk.TripPositionId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Driver)
                    .WithMany(r => r.DriverTripPositions)
                    .HasForeignKey(fk => fk.DriverId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasAlternateKey(k => new {k.TripPositionId, k.PositionId});

                b.HasKey(k => k.Id);
                b.ToTable("DriverTripPositions");
            });

        }
    }
}
