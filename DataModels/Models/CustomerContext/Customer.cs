using System;
using System.Collections.Generic;
using DataModels.CustomerToDriverReviewContext;
using DataModels.CustomerTripPositionContext;
using DataModels.DriverToCustomerReviewContext;
using DataModels.TripContext;

namespace DataModels.CustomerContext
{
    public class Customer
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;

        public ICollection<Trip> Trips { get; set; }

        public ICollection<DriverToCustomerReview> DriverToCustomerReviews { get; set; }
        public ICollection<CustomerToDriverReview> CustomerToDriverReviews { get; set; }
        public ICollection<CustomerTripPosition> CustomerTripPositions { get; set; }
    }
}
