using Microsoft.EntityFrameworkCore;

namespace DataModels.CustomerContext
{
    public class CustomerConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<Customer>(b =>
            {
                b.Property(customer => customer.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.Property(customer => customer.FirstName)
                    .HasMaxLength(160)
                    .IsRequired();
                b.Property(customer => customer.LastName)
                    .HasMaxLength(160)
                    .IsRequired();

                b.Property(customer => customer.Email)
                    .HasMaxLength(320)
                    .IsRequired();

                b.HasIndex(i => i.Email)
                    .IsUnique();

                b.HasKey(k => k.Id);
                b.ToTable("Customers");
            });

        }
    }
}
