using System;
using System.Collections.Generic;
using DataModels.CustomerTripPositionContext;
using DataModels.DriverTripPositionContext;
using DataModels.TripContext;
using DataModels.CoordinateContext;

namespace DataModels.PositionContext
{
    public class Position
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public double Accuracy { get; set; }
        public double AltitudeAccuracy { get; set; }
        public double Heading { get; set; }
        public double Speed { get; set; } = 0.0;

        public Guid CoordinateId { get; set; }
        public Coordinate Coordinate { get; set; }

        public Trip TripOrigin { get; set; }
        public Trip TripDestination { get; set; }

        public ICollection<CustomerTripPosition> CustomerTripPositions { get; set; }
        public ICollection<DriverTripPosition> DriverTripPositions { get; set; }
    }
}
