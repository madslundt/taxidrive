using Microsoft.EntityFrameworkCore;

namespace DataModels.PositionContext
{
    public class PositionConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<Position>(b =>
            {
                b.Property(position => position.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.HasOne(r => r.Coordinate)
                    .WithOne(r => r.Position)
                    .HasForeignKey<Position>(fk => fk.CoordinateId);

                b.HasKey(k => k.Id);
                b.ToTable("Positions");
            });

        }
    }
}
