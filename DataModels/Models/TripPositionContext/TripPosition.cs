using System;
using System.Collections.Generic;
using DataModels.TripContext;
using DataModels.CustomerTripPositionContext;
using DataModels.DriverTripPositionContext;

namespace DataModels.TripPositionContext
{
    public class TripPosition
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid TripId { get; set; }
        public Trip Trip { get; set; }
        public ICollection<CustomerTripPosition> CustomerTripPositions { get; set; }
        public ICollection<DriverTripPosition> DriverTripPositions { get; set; }

        public TripStatus Status { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
    }

    public class TripStatusRef
    {
        public TripStatus Id { get; set; }
        public string Name { get; set; }
    }

    public enum TripStatus
    {
        Requesting,
        Accepted,
        Ready,
        Driving,
        Done,
        CustomerCancelled,
        DriverCancelled
    }
}
