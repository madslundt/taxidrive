using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataModels.TripPositionContext
{
    public class TripPositionConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<TripPosition>(b =>
            {
                b.Property(tripPosition => tripPosition.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.HasOne(r => r.Trip)
                    .WithMany(r => r.Positions)
                    .HasForeignKey(fk => fk.TripId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.Property(position => position.Status)
                    .HasDefaultValueSql(((int) TripStatus.Requesting).ToString())
                    .IsRequired();

                b.HasAlternateKey(k => new {k.TripId, k.Status});

                b.HasKey(k => k.Id);
                b.ToTable("TripPositions");
            });

            builder.Entity<TripStatusRef>(b =>
            {
                b.Property(status => status.Name)
                    .IsRequired();

                b.HasKey(k => k.Id);
                b.ToTable("TripStatusReferences");
            });

        }
    }
}
