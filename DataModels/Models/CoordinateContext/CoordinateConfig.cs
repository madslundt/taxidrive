using Microsoft.EntityFrameworkCore;

namespace DataModels.CoordinateContext
{
    public class CoordinateConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<Coordinate>(b =>
            {
                b.Property(coordinate => coordinate.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.Property(coordinate => coordinate.Latitude)
                    .IsRequired();
                b.Property(coordinate => coordinate.Longitude)
                    .IsRequired();

                b.HasKey(k => k.Id);
                b.ToTable("Coordinates");
            });

        }
    }
}
