using System;
using DataModels.PositionContext;
using DataModels.TripContext;

namespace DataModels.CoordinateContext
{
    public class Coordinate
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public double Altitude { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Trip TripOrigin { get; set; }
        public Trip TripDestination { get; set; }
        public Position Position { get; set; }
    }
}
