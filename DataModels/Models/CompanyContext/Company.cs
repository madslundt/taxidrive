using System;
using System.Collections.Generic;
using DataModels.DriverContext;

namespace DataModels.CompanyContext
{
    public class Company
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public ICollection<Driver> Drivers { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
    }
}
