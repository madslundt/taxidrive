using Microsoft.EntityFrameworkCore;

namespace DataModels.CompanyContext
{
    public class CompanyConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<Company>(b =>
            {
                b.Property(company => company.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.Property(company => company.Name)
                    .HasMaxLength(320)
                    .IsRequired();

                b.HasKey(k => k.Id);
                b.ToTable("Companies");
            });

        }
    }
}
