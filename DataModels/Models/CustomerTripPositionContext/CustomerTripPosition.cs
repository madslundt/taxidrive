using System;
using DataModels.PositionContext;
using DataModels.TripPositionContext;
using DataModels.CustomerContext;

namespace DataModels.CustomerTripPositionContext
{
    public class CustomerTripPosition
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid TripPositionId { get; set; }
        public TripPosition TripPosition { get; set; }
        public Guid PositionId { get; set; }
        public Position Position { get; set; }
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
    }
}
