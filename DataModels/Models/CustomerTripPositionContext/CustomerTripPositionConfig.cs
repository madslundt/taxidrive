using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataModels.CustomerTripPositionContext
{
    public class CustomerTripPositionConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<CustomerTripPosition>(b =>
            {
                b.Property(position => position.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.HasOne(r => r.Position)
                    .WithMany(r => r.CustomerTripPositions)
                    .HasForeignKey(fk => fk.PositionId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.TripPosition)
                    .WithMany(r => r.CustomerTripPositions)
                    .HasForeignKey(fk => fk.TripPositionId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Customer)
                    .WithMany(r => r.CustomerTripPositions)
                    .HasForeignKey(fk => fk.CustomerId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasAlternateKey(k => new {k.TripPositionId, k.PositionId});

                b.HasKey(k => k.Id);
                b.ToTable("CustomerTripPositions");
            });

        }
    }
}
