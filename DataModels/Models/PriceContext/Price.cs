using System;
using DataModels.TripContext;

namespace DataModels.PriceContext
{
    public class Price
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public decimal Value { get; set; }
        public Currency Currency { get; set; }

        public Trip Trip { get; set; }
    }

    public class CurrencyRef
    {
        public Currency Id { get; set; }
        public string Name { get; set; }
    }

    public enum Currency
    {
        euro = 1,
        dkk = 2
    }
}
