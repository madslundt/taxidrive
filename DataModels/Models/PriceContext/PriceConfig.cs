using Microsoft.EntityFrameworkCore;

namespace DataModels.PriceContext
{
    public class PriceConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<Price>(b =>
            {
                b.Property(price => price.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.Property(price => price.Currency)
                    .IsRequired();

                b.HasKey(k => k.Id);
                b.ToTable("Prices");
            });

            builder.Entity<CurrencyRef>(b =>
            {
                b.Property(currency => currency.Name)
                    .IsRequired();

                b.HasKey(k => k.Id);
                b.ToTable("CurrencyReferences");
            });

        }
    }
}
