using System;
using System.Collections.Generic;
using DataModels.CoordinateContext;
using DataModels.CustomerContext;
using DataModels.CustomerToDriverReviewContext;
using DataModels.DriverContext;
using DataModels.DriverToCustomerReviewContext;
using DataModels.PositionContext;
using DataModels.PriceContext;
using DataModels.TripPositionContext;

namespace DataModels.TripContext
{
    public class Trip
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }

        public Guid DriverId { get; set; }
        public Driver Driver { get; set; }

        public Guid OriginId { get; set; }
        public Coordinate Origin { get; set; }
        public Guid DestinationId { get; set; }
        public Coordinate Destination { get; set; }

        public Guid PriceId { get; set; }
        public Price Price { get; set; }

        public ICollection<TripPosition> Positions { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;

        public DriverToCustomerReview DriverToCustomerReview { get; set; }
        public CustomerToDriverReview CustomerToDriverReview { get; set; }
    }
}
