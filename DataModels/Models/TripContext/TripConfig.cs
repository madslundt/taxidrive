using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataModels.TripContext
{
    public class TripConfig
    {
        public static void Build(ModelBuilder builder)
        {
            builder.Entity<Trip>(b =>
            {
                b.Property(trip => trip.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                b.Property(trip => trip.CustomerId)
                    .IsRequired();
                b.Property(trip => trip.DriverId)
                    .IsRequired();

                b.HasOne(r => r.Customer)
                    .WithMany(r => r.Trips)
                    .HasForeignKey(fk => fk.CustomerId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Driver)
                    .WithMany(r => r.Trips)
                    .HasForeignKey(fk => fk.DriverId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Origin)
                    .WithOne(r => r.TripOrigin)
                    .HasForeignKey<Trip>(fk => fk.OriginId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Destination)
                    .WithOne(r => r.TripDestination)
                    .HasForeignKey<Trip>(fk => fk.DestinationId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(r => r.Price)
                    .WithOne(r => r.Trip)
                    .HasForeignKey<Trip>(fk => fk.PriceId)
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasAlternateKey(k => new {k.CustomerId, k.DriverId});

                b.HasKey(k => k.Id);
                b.ToTable("Trips");
            });
        }
    }
}
