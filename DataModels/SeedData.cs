using System;
using System.Collections.Generic;
using System.Linq;
using DataModels.PriceContext;
using DataModels.TripPositionContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace DataModels
{
    public static class DataSeeder
    {
        // TODO: Move this code when seed data is implemented in EF 7

        /// <summary>
        /// This is a workaround for missing seed data functionality in EF 7.0-rc1
        /// More info: https://github.com/aspnet/EntityFramework/issues/629
        /// </summary>
        /// <param name="app">
        /// An instance that provides the mechanisms to get instance of the database context.
        /// </param>
        public static void SeedData(this IApplicationBuilder app)
        {
            var db = app.ApplicationServices.GetService<ApplicationContext>();

            SeedTripStatusRef(db);
            SeedCurrencyRef(db);

            db.SaveChanges();
        }

        private static void SeedTripStatusRef(ApplicationContext db)
        {
            bool shouldSeedData = db.TripStatuses.Count() == 0;
            
            if (shouldSeedData)
            {
                var list = new List<TripStatusRef>();

                foreach (TripStatus status in Enum.GetValues(typeof(TripStatus)))
                {
                    list.Add(
                        new TripStatusRef
                        {
                            Id = status,
                            Name = status.ToString()
                        }
                    );
                }

                db.AddRange(list);
            }
        }

        private static void SeedCurrencyRef(ApplicationContext db)
        {
            bool shouldSeedData = db.Currencies.Count() == 0;
            
            if (shouldSeedData)
            {
                var list = new List<CurrencyRef>();

                foreach (Currency status in Enum.GetValues(typeof(Currency)))
                {
                    list.Add(
                        new CurrencyRef
                        {
                            Id = status,
                            Name = status.ToString()
                        }
                    );
                }

                db.AddRange(list);
            }
        }
    }
}