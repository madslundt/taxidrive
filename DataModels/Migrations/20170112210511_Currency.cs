﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModels.Migrations
{
    public partial class Currency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Price_PriceId",
                table: "Trips");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Price",
                table: "Price");

            migrationBuilder.RenameTable(
                name: "Price",
                newName: "Prices");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "TripPositions",
                nullable: false,
                defaultValueSql: "0",
                oldClrType: typeof(int),
                oldDefaultValueSql: "1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Prices",
                table: "Prices",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "CurrencyReferences",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyReferences", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Prices_PriceId",
                table: "Trips",
                column: "PriceId",
                principalTable: "Prices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Prices_PriceId",
                table: "Trips");

            migrationBuilder.DropTable(
                name: "CurrencyReferences");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Prices",
                table: "Prices");

            migrationBuilder.RenameTable(
                name: "Prices",
                newName: "Price");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "TripPositions",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(int),
                oldDefaultValueSql: "0");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Price",
                table: "Price",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Price_PriceId",
                table: "Trips",
                column: "PriceId",
                principalTable: "Price",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
