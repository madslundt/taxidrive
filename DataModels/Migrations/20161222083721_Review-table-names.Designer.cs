﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using DataModels;
using DataModels.TripPositionContext;

namespace DataModels.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    [Migration("20161222083721_Review-table-names")]
    partial class Reviewtablenames
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("DataModels.CompanyContext.Company", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(320);

                    b.HasKey("Id");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("DataModels.CustomerContext.Customer", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(320);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(160);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(160);

                    b.HasKey("Id");

                    b.HasIndex("Email");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("DataModels.CustomerToDriverReviewContext.CustomerToDriverReview", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<Guid>("CreatorId");

                    b.Property<Guid>("DriverId");

                    b.Property<int>("Rating");

                    b.Property<string>("Text");

                    b.Property<Guid>("TripId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TripId", "CreatorId", "DriverId");

                    b.HasIndex("CreatorId");

                    b.HasIndex("DriverId");

                    b.HasIndex("TripId")
                        .IsUnique();

                    b.ToTable("CustomerToDriverReviews");
                });

            modelBuilder.Entity("DataModels.CustomerTripPositionContext.CustomerTripPosition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<Guid>("CustomerId");

                    b.Property<Guid>("PositionId");

                    b.Property<Guid>("TripPositionId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TripPositionId", "PositionId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("PositionId");

                    b.ToTable("CustomerTripPositions");
                });

            modelBuilder.Entity("DataModels.DriverContext.Driver", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CompanyId");

                    b.Property<DateTime>("Created");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(320);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(160);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(160);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("Email");

                    b.ToTable("Drivers");
                });

            modelBuilder.Entity("DataModels.DriverToCustomerReviewContext.DriverToCustomerReview", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<Guid>("CreatorId");

                    b.Property<Guid>("CustomerId");

                    b.Property<int>("Rating");

                    b.Property<string>("Text");

                    b.Property<Guid>("TripId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TripId", "CustomerId", "CreatorId");

                    b.HasIndex("CreatorId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("TripId")
                        .IsUnique();

                    b.ToTable("DriverToCustomerReviews");
                });

            modelBuilder.Entity("DataModels.DriverTripPositionContext.DriverTripPosition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<Guid>("DriverId");

                    b.Property<Guid>("PositionId");

                    b.Property<Guid>("TripPositionId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TripPositionId", "PositionId");

                    b.HasIndex("DriverId");

                    b.HasIndex("PositionId");

                    b.ToTable("DriverTripPositions");
                });

            modelBuilder.Entity("DataModels.PositionContext.Position", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Accuracy");

                    b.Property<double>("Altitude");

                    b.Property<double>("AltitudeAccuracy");

                    b.Property<double>("Heading");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<double>("Speed");

                    b.HasKey("Id");

                    b.ToTable("Positions");
                });

            modelBuilder.Entity("DataModels.TripContext.Trip", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<Guid>("CustomerId");

                    b.Property<Guid>("DestinationId");

                    b.Property<Guid>("DriverId");

                    b.Property<Guid>("OriginId");

                    b.HasKey("Id");

                    b.HasAlternateKey("CustomerId", "DriverId");

                    b.HasIndex("DestinationId")
                        .IsUnique();

                    b.HasIndex("DriverId");

                    b.HasIndex("OriginId")
                        .IsUnique();

                    b.ToTable("Trips");
                });

            modelBuilder.Entity("DataModels.TripPositionContext.TripPosition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<int>("Status")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("1");

                    b.Property<Guid>("TripId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TripId", "Status");

                    b.ToTable("TripPositions");
                });

            modelBuilder.Entity("DataModels.TripPositionContext.TripStatusRef", b =>
                {
                    b.Property<int>("Id");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("TripStatusReferences");
                });

            modelBuilder.Entity("DataModels.CustomerToDriverReviewContext.CustomerToDriverReview", b =>
                {
                    b.HasOne("DataModels.CustomerContext.Customer", "Creator")
                        .WithMany("CustomerToDriverReviews")
                        .HasForeignKey("CreatorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DataModels.DriverContext.Driver", "Driver")
                        .WithMany("CustomerToDriverReviews")
                        .HasForeignKey("DriverId");

                    b.HasOne("DataModels.TripContext.Trip", "Trip")
                        .WithOne("CustomerToDriverReview")
                        .HasForeignKey("DataModels.CustomerToDriverReviewContext.CustomerToDriverReview", "TripId");
                });

            modelBuilder.Entity("DataModels.CustomerTripPositionContext.CustomerTripPosition", b =>
                {
                    b.HasOne("DataModels.CustomerContext.Customer", "Customer")
                        .WithMany("CustomerTripPositions")
                        .HasForeignKey("CustomerId");

                    b.HasOne("DataModels.PositionContext.Position", "Position")
                        .WithMany("CustomerTripPositions")
                        .HasForeignKey("PositionId");

                    b.HasOne("DataModels.TripPositionContext.TripPosition", "TripPosition")
                        .WithMany("CustomerTripPositions")
                        .HasForeignKey("TripPositionId");
                });

            modelBuilder.Entity("DataModels.DriverContext.Driver", b =>
                {
                    b.HasOne("DataModels.CompanyContext.Company", "Company")
                        .WithMany("Drivers")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DataModels.DriverToCustomerReviewContext.DriverToCustomerReview", b =>
                {
                    b.HasOne("DataModels.DriverContext.Driver", "Creator")
                        .WithMany("DriverToCustomerReviews")
                        .HasForeignKey("CreatorId");

                    b.HasOne("DataModels.CustomerContext.Customer", "Customer")
                        .WithMany("DriverToCustomerReviews")
                        .HasForeignKey("CustomerId");

                    b.HasOne("DataModels.TripContext.Trip", "Trip")
                        .WithOne("DriverToCustomerReview")
                        .HasForeignKey("DataModels.DriverToCustomerReviewContext.DriverToCustomerReview", "TripId");
                });

            modelBuilder.Entity("DataModels.DriverTripPositionContext.DriverTripPosition", b =>
                {
                    b.HasOne("DataModels.DriverContext.Driver", "Driver")
                        .WithMany("DriverTripPositions")
                        .HasForeignKey("DriverId");

                    b.HasOne("DataModels.PositionContext.Position", "Position")
                        .WithMany("DriverTripPositions")
                        .HasForeignKey("PositionId");

                    b.HasOne("DataModels.TripPositionContext.TripPosition", "TripPosition")
                        .WithMany("DriverTripPositions")
                        .HasForeignKey("TripPositionId");
                });

            modelBuilder.Entity("DataModels.TripContext.Trip", b =>
                {
                    b.HasOne("DataModels.CustomerContext.Customer", "Customer")
                        .WithMany("Trips")
                        .HasForeignKey("CustomerId");

                    b.HasOne("DataModels.PositionContext.Position", "Destination")
                        .WithOne("TripDestination")
                        .HasForeignKey("DataModels.TripContext.Trip", "DestinationId");

                    b.HasOne("DataModels.DriverContext.Driver", "Driver")
                        .WithMany("Trips")
                        .HasForeignKey("DriverId");

                    b.HasOne("DataModels.PositionContext.Position", "Origin")
                        .WithOne("TripOrigin")
                        .HasForeignKey("DataModels.TripContext.Trip", "OriginId");
                });

            modelBuilder.Entity("DataModels.TripPositionContext.TripPosition", b =>
                {
                    b.HasOne("DataModels.TripContext.Trip", "Trip")
                        .WithMany("Positions")
                        .HasForeignKey("TripId");
                });
        }
    }
}
