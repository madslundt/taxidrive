﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModels.Migrations
{
    public partial class Drivercompanies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerReviews_Drivers_DriverId",
                table: "CustomerReviews");

            migrationBuilder.DropForeignKey(
                name: "FK_Drivers_Companies_Id",
                table: "Drivers");

            migrationBuilder.DropForeignKey(
                name: "FK_DriverReviews_Customers_CustomerId",
                table: "DriverReviews");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_DriverReviews_TripId_CustomerId_DriverId",
                table: "DriverReviews");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_CustomerReviews_TripId_CustomerId_DriverId",
                table: "CustomerReviews");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "DriverReviews",
                newName: "CreatorId");

            migrationBuilder.RenameIndex(
                name: "IX_DriverReviews_CustomerId",
                table: "DriverReviews",
                newName: "IX_DriverReviews_CreatorId");

            migrationBuilder.RenameColumn(
                name: "Lastname",
                table: "Drivers",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "Firstname",
                table: "Drivers",
                newName: "FirstName");

            migrationBuilder.RenameColumn(
                name: "DriverId",
                table: "CustomerReviews",
                newName: "CreatorId");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerReviews_DriverId",
                table: "CustomerReviews",
                newName: "IX_CustomerReviews_CreatorId");

            migrationBuilder.RenameColumn(
                name: "Lastname",
                table: "Customers",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "Firstname",
                table: "Customers",
                newName: "FirstName");

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyId",
                table: "Drivers",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddUniqueConstraint(
                name: "AK_DriverReviews_TripId_CreatorId_DriverId",
                table: "DriverReviews",
                columns: new[] { "TripId", "CreatorId", "DriverId" });

            migrationBuilder.AddUniqueConstraint(
                name: "AK_CustomerReviews_TripId_CustomerId_CreatorId",
                table: "CustomerReviews",
                columns: new[] { "TripId", "CustomerId", "CreatorId" });

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_CompanyId",
                table: "Drivers",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerReviews_Drivers_CreatorId",
                table: "CustomerReviews",
                column: "CreatorId",
                principalTable: "Drivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Drivers_Companies_CompanyId",
                table: "Drivers",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DriverReviews_Customers_CreatorId",
                table: "DriverReviews",
                column: "CreatorId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerReviews_Drivers_CreatorId",
                table: "CustomerReviews");

            migrationBuilder.DropForeignKey(
                name: "FK_Drivers_Companies_CompanyId",
                table: "Drivers");

            migrationBuilder.DropForeignKey(
                name: "FK_DriverReviews_Customers_CreatorId",
                table: "DriverReviews");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_DriverReviews_TripId_CreatorId_DriverId",
                table: "DriverReviews");

            migrationBuilder.DropIndex(
                name: "IX_Drivers_CompanyId",
                table: "Drivers");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_CustomerReviews_TripId_CustomerId_CreatorId",
                table: "CustomerReviews");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Drivers");

            migrationBuilder.RenameColumn(
                name: "CreatorId",
                table: "DriverReviews",
                newName: "CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_DriverReviews_CreatorId",
                table: "DriverReviews",
                newName: "IX_DriverReviews_CustomerId");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Drivers",
                newName: "Lastname");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Drivers",
                newName: "Firstname");

            migrationBuilder.RenameColumn(
                name: "CreatorId",
                table: "CustomerReviews",
                newName: "DriverId");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerReviews_CreatorId",
                table: "CustomerReviews",
                newName: "IX_CustomerReviews_DriverId");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Customers",
                newName: "Lastname");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Customers",
                newName: "Firstname");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_DriverReviews_TripId_CustomerId_DriverId",
                table: "DriverReviews",
                columns: new[] { "TripId", "CustomerId", "DriverId" });

            migrationBuilder.AddUniqueConstraint(
                name: "AK_CustomerReviews_TripId_CustomerId_DriverId",
                table: "CustomerReviews",
                columns: new[] { "TripId", "CustomerId", "DriverId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerReviews_Drivers_DriverId",
                table: "CustomerReviews",
                column: "DriverId",
                principalTable: "Drivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Drivers_Companies_Id",
                table: "Drivers",
                column: "Id",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DriverReviews_Customers_CustomerId",
                table: "DriverReviews",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
