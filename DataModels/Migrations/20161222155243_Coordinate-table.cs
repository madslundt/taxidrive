﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModels.Migrations
{
    public partial class Coordinatetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Positions_DestinationId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Positions_OriginId",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "Altitude",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "Positions");

            migrationBuilder.AddColumn<Guid>(
                name: "CoordinateId",
                table: "Positions",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TripDestinationId",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TripOriginId",
                table: "Positions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Coodinates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Altitude = table.Column<double>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coodinates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Positions_CoordinateId",
                table: "Positions",
                column: "CoordinateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Positions_TripDestinationId",
                table: "Positions",
                column: "TripDestinationId");

            migrationBuilder.CreateIndex(
                name: "IX_Positions_TripOriginId",
                table: "Positions",
                column: "TripOriginId");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Coodinates_CoordinateId",
                table: "Positions",
                column: "CoordinateId",
                principalTable: "Coodinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Trips_TripDestinationId",
                table: "Positions",
                column: "TripDestinationId",
                principalTable: "Trips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Trips_TripOriginId",
                table: "Positions",
                column: "TripOriginId",
                principalTable: "Trips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Coodinates_DestinationId",
                table: "Trips",
                column: "DestinationId",
                principalTable: "Coodinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Coodinates_OriginId",
                table: "Trips",
                column: "OriginId",
                principalTable: "Coodinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Coodinates_CoordinateId",
                table: "Positions");

            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Trips_TripDestinationId",
                table: "Positions");

            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Trips_TripOriginId",
                table: "Positions");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Coodinates_DestinationId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Coodinates_OriginId",
                table: "Trips");

            migrationBuilder.DropTable(
                name: "Coodinates");

            migrationBuilder.DropIndex(
                name: "IX_Positions_CoordinateId",
                table: "Positions");

            migrationBuilder.DropIndex(
                name: "IX_Positions_TripDestinationId",
                table: "Positions");

            migrationBuilder.DropIndex(
                name: "IX_Positions_TripOriginId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "CoordinateId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "TripDestinationId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "TripOriginId",
                table: "Positions");

            migrationBuilder.AddColumn<double>(
                name: "Altitude",
                table: "Positions",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "Positions",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "Positions",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Positions_DestinationId",
                table: "Trips",
                column: "DestinationId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Positions_OriginId",
                table: "Trips",
                column: "OriginId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
