﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModels.Migrations
{
    public partial class TripStatusReference : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TripPositions_TripStatusReferences_TripStatusId",
                table: "TripPositions");

            migrationBuilder.DropIndex(
                name: "IX_TripPositions_TripStatusId",
                table: "TripPositions");

            migrationBuilder.DropColumn(
                name: "TripStatusId",
                table: "TripPositions");

            migrationBuilder.DropColumn(
                name: "TripId",
                table: "Drivers");

            migrationBuilder.DropColumn(
                name: "TripId",
                table: "Customers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TripStatusId",
                table: "TripPositions",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TripId",
                table: "Drivers",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TripId",
                table: "Customers",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_TripPositions_TripStatusId",
                table: "TripPositions",
                column: "TripStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_TripPositions_TripStatusReferences_TripStatusId",
                table: "TripPositions",
                column: "TripStatusId",
                principalTable: "TripStatusReferences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
