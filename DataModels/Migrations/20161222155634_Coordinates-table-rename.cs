﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModels.Migrations
{
    public partial class Coordinatestablerename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Coodinates_CoordinateId",
                table: "Positions");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Coodinates_DestinationId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Coodinates_OriginId",
                table: "Trips");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Coodinates",
                table: "Coodinates");

            migrationBuilder.RenameTable(
                name: "Coodinates",
                newName: "Coordinates");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Coordinates",
                table: "Coordinates",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Coordinates_CoordinateId",
                table: "Positions",
                column: "CoordinateId",
                principalTable: "Coordinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Coordinates_DestinationId",
                table: "Trips",
                column: "DestinationId",
                principalTable: "Coordinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Coordinates_OriginId",
                table: "Trips",
                column: "OriginId",
                principalTable: "Coordinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Coordinates_CoordinateId",
                table: "Positions");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Coordinates_DestinationId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Coordinates_OriginId",
                table: "Trips");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Coordinates",
                table: "Coordinates");

            migrationBuilder.RenameTable(
                name: "Coordinates",
                newName: "Coodinates");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Coodinates",
                table: "Coodinates",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Coodinates_CoordinateId",
                table: "Positions",
                column: "CoordinateId",
                principalTable: "Coodinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Coodinates_DestinationId",
                table: "Trips",
                column: "DestinationId",
                principalTable: "Coodinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Coodinates_OriginId",
                table: "Trips",
                column: "OriginId",
                principalTable: "Coodinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
