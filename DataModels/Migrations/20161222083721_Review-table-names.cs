﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModels.Migrations
{
    public partial class Reviewtablenames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerReviews");

            migrationBuilder.DropTable(
                name: "DriverReviews");

            migrationBuilder.CreateTable(
                name: "CustomerToDriverReviews",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: false),
                    DriverId = table.Column<Guid>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    TripId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerToDriverReviews", x => x.Id);
                    table.UniqueConstraint("AK_CustomerToDriverReviews_TripId_CreatorId_DriverId", x => new { x.TripId, x.CreatorId, x.DriverId });
                    table.ForeignKey(
                        name: "FK_CustomerToDriverReviews_Customers_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerToDriverReviews_Drivers_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerToDriverReviews_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverToCustomerReviews",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: false),
                    CustomerId = table.Column<Guid>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    TripId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverToCustomerReviews", x => x.Id);
                    table.UniqueConstraint("AK_DriverToCustomerReviews_TripId_CustomerId_CreatorId", x => new { x.TripId, x.CustomerId, x.CreatorId });
                    table.ForeignKey(
                        name: "FK_DriverToCustomerReviews_Drivers_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverToCustomerReviews_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverToCustomerReviews_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerToDriverReviews_CreatorId",
                table: "CustomerToDriverReviews",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerToDriverReviews_DriverId",
                table: "CustomerToDriverReviews",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerToDriverReviews_TripId",
                table: "CustomerToDriverReviews",
                column: "TripId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DriverToCustomerReviews_CreatorId",
                table: "DriverToCustomerReviews",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverToCustomerReviews_CustomerId",
                table: "DriverToCustomerReviews",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverToCustomerReviews_TripId",
                table: "DriverToCustomerReviews",
                column: "TripId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerToDriverReviews");

            migrationBuilder.DropTable(
                name: "DriverToCustomerReviews");

            migrationBuilder.CreateTable(
                name: "CustomerReviews",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: false),
                    CustomerId = table.Column<Guid>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    TripId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerReviews", x => x.Id);
                    table.UniqueConstraint("AK_CustomerReviews_TripId_CustomerId_CreatorId", x => new { x.TripId, x.CustomerId, x.CreatorId });
                    table.ForeignKey(
                        name: "FK_CustomerReviews_Drivers_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerReviews_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerReviews_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverReviews",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: false),
                    DriverId = table.Column<Guid>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    TripId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverReviews", x => x.Id);
                    table.UniqueConstraint("AK_DriverReviews_TripId_CreatorId_DriverId", x => new { x.TripId, x.CreatorId, x.DriverId });
                    table.ForeignKey(
                        name: "FK_DriverReviews_Customers_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverReviews_Drivers_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverReviews_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerReviews_CreatorId",
                table: "CustomerReviews",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerReviews_CustomerId",
                table: "CustomerReviews",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerReviews_TripId",
                table: "CustomerReviews",
                column: "TripId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DriverReviews_CreatorId",
                table: "DriverReviews",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverReviews_DriverId",
                table: "DriverReviews",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverReviews_TripId",
                table: "DriverReviews",
                column: "TripId",
                unique: true);
        }
    }
}
