using Microsoft.EntityFrameworkCore;
using DataModels.CustomerContext;
using DataModels.CustomerToDriverReviewContext;
using DataModels.DriverContext;
using DataModels.DriverToCustomerReviewContext;
using DataModels.PositionContext;
using DataModels.TripContext;
using DataModels.CompanyContext;
using DataModels.TripPositionContext;
using DataModels.CustomerTripPositionContext;
using DataModels.DriverTripPositionContext;
using DataModels.CoordinateContext;
using DataModels.PriceContext;

namespace DataModels
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        public ApplicationContext()
        {
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<TripStatusRef> TripStatuses { get; set; }
        public DbSet<CustomerToDriverReview> CustomerToDriverReviews { get; set; }
        public DbSet<DriverToCustomerReview> DriverToCustomerReviews { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Coordinate> Coordinates { get; set; }
        public DbSet<TripPosition> TripPositions { get; set; }
        public DbSet<CustomerTripPosition> CustomerTripPosition { get; set; }
        public DbSet<DriverTripPosition> DriverTripPositions { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<CurrencyRef> Currencies { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            CompanyConfig.Build(builder);
            CustomerConfig.Build(builder);
            DriverConfig.Build(builder);
            TripConfig.Build(builder);
            CustomerToDriverReviewConfig.Build(builder);
            DriverToCustomerReviewConfig.Build(builder);
            PositionConfig.Build(builder);
            TripPositionConfig.Build(builder);
            CustomerTripPositionConfig.Build(builder);
            DriverTripPositionConfig.Build(builder);
            CoordinateConfig.Build(builder);
            PriceConfig.Build(builder);
        }
    }
}
