using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Source;
using Shouldly;
using Xunit;

namespace BoundaryTests.Company
{
    // see example explanation on xUnit.net website:
    // https://xunit.github.io/docs/getting-started-dotnet-core.html
    public class CompanyControllerTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public CompanyControllerTests()
        {
            var hostBuilder = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory() + "/../Source")
                .UseStartup<Startup>();

            _server = new TestServer(hostBuilder);
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task ItShouldReturnNotFoundWhenCompanyDoesNotExist()
        {
            Guid guid = Guid.NewGuid();
            var response = await _client.GetAsync($"/api/companies/{guid}");

            response.StatusCode.ShouldBe(System.Net.HttpStatusCode.NotFound);
        }
        [Fact]
        public async Task ItShouldReturnBadRequestWhenCompanyIdIsEmpty()
        {
            Guid guid = Guid.Empty;

            var response = await _client.GetAsync($"/api/companies/{guid}");

            response.StatusCode.ShouldBe(System.Net.HttpStatusCode.BadRequest);
        }
    }
}
