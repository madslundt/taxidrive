using Bogus;
using FluentValidation;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Customer;
using Shouldly;
using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace IntegrationTests.Features.Customer
{
    public class AddCustomerTests
    {
        private readonly IMediator _mediator;
        private readonly ApplicationContext _db;
        public AddCustomerTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            var builder = new ConfigurationBuilder().AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.json");
            var configuration = builder.Build();

            services.AddDbContext<ApplicationContext>(
                options => options.UseNpgsql(configuration["DataSettings:ConnectionString"])
            );

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _db = container.GetInstance<ApplicationContext>();
            _mediator = container.GetInstance<IMediator>();
        }

        // [Fact] TODO
        public async Task ItShouldThrowArgumentNullExceptionWhenEmailAlreadyExist()
        {
            Randomizer.Seed = new Random(3897234);
            DataModels.CustomerContext.Customer customer = new Faker<DataModels.CustomerContext.Customer>()
                .RuleFor(c => c.Email, f => f.Person.Email)
                .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                .RuleFor(c => c.LastName, f => f.Person.LastName)
                .Generate();

            _db.Add(customer);
            await _db.SaveChangesAsync();

            AddCustomer.Request request = new Faker<AddCustomer.Request>()
                .RuleFor(c => c.Email, f => customer.Email)
                .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                .RuleFor(c => c.LastName, f => f.Person.LastName)
                .RuleFor(c => c.Password, f => f.Person.FirstName)
                .Generate();

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnCustomerWhenExist()
        {
            Randomizer.Seed = new Random(3897234);
            AddCustomer.Request request = new Faker<AddCustomer.Request>()
                .RuleFor(c => c.Email, f => f.Person.Email)
                .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                .RuleFor(c => c.LastName, f => f.Person.LastName)
                .RuleFor(c => c.Password, f => f.Person.FirstName)
                .Generate();

            AddCustomer.Response actualCustomer = await _mediator.SendAsync(request).ConfigureAwait(false);

            actualCustomer.ShouldNotBeNull();
            actualCustomer.Id.ShouldNotBe(Guid.Empty);
        }
    }
}
