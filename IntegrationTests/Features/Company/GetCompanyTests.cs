using Bogus;
using FluentValidation;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Company;
using Shouldly;
using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace IntegrationTests.Features.Company
{

    public class GetCompanyTests
    {
        private readonly IMediator _mediator;
        private readonly ApplicationContext _db;
        public GetCompanyTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            var builder = new ConfigurationBuilder().AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.json");
            var configuration = builder.Build();

            services.AddDbContext<ApplicationContext>(
                options => options.UseNpgsql(configuration["DataSettings:ConnectionString"])
            );

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _db = container.GetInstance<ApplicationContext>();
            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenCompanyDoesNotExist()
        {
            Randomizer.Seed = new Random(3897234);
            GetCompany.Request request = new Faker<GetCompany.Request>()
                .RuleFor(c => c.CompanyId, f => Guid.NewGuid())
                .Generate();

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnCompanyWhenExist()
        {
            Randomizer.Seed = new Random(3897234);
            DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                .RuleFor(c => c.Name, f => f.Company.CompanyName())
                .Generate();

            _db.Add(company);
            await _db.SaveChangesAsync();

            GetCompany.Request request = new GetCompany.Request
            {
                CompanyId = company.Id
            };

            GetCompany.Response actualCompany = await _mediator.SendAsync(request).ConfigureAwait(false);

            actualCompany.Id.ShouldBe(company.Id);
            actualCompany.Name.ShouldBe(company.Name);
        }
    }
}
