using Bogus;
using FluentValidation;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Driver;
using Shouldly;
using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace IntegrationTests.Features.Driver
{
    public class GetDriverTests
    {
        private readonly IMediator _mediator;
        private readonly ApplicationContext _db;
        public GetDriverTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            var builder = new ConfigurationBuilder().AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.json");
            var configuration = builder.Build();

            services.AddDbContext<ApplicationContext>(
                options => options.UseNpgsql(configuration["DataSettings:ConnectionString"])
            );

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _db = container.GetInstance<ApplicationContext>();
            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldThrowArgumentNullExceptionWhenDriverDoesNotExist()
        {
            Randomizer.Seed = new Random(3897234);
            GetDriver.Request request = new Faker<GetDriver.Request>()
                .RuleFor(c => c.DriverId, f => Guid.NewGuid())
                .Generate();

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await _mediator.SendAsync(request).ConfigureAwait(false)
            ).ConfigureAwait(false);
        }
        [Fact]
        public async Task ItShouldReturnDriverWhenExist()
        {
            Randomizer.Seed = new Random(3897234);
            DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

            DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                .RuleFor(c => c.LastName, f => f.Person.LastName)
                .RuleFor(c => c.Email, f => f.Person.Email)
                .RuleFor(c => c.Company, f => company)
                .Generate();

            _db.Add(company);
            _db.Add(driver);
            await _db.SaveChangesAsync();

            GetDriver.Request request = new GetDriver.Request
            {
                DriverId = driver.Id
            };

            GetDriver.Response actualDriver = await _mediator.SendAsync(request).ConfigureAwait(false);

            actualDriver.Id.ShouldBe(driver.Id);
            actualDriver.FirstName.ShouldBe(driver.FirstName);
            actualDriver.LastName.ShouldBe(driver.LastName);
            actualDriver.Email.ShouldBe(driver.Email);
        }
    }
}
