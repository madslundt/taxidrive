using Bogus;
using FluentValidation;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using Xunit;
using NSubstitute;
using DataModels;
using Source.Infrastructure.MessageProcessing;
using Source.Features.Driver;
using Shouldly;
using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace IntegrationTests.Features.Driver
{
    public class GetCompanyDriversTests
    {
        private readonly IMediator _mediator;
        private readonly ApplicationContext _db;
        public GetCompanyDriversTests()
        {
            var httpCtx = Substitute.For<IHttpContextAccessor>();
            var services = new ServiceCollection();
            var builder = new ConfigurationBuilder().AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.json");
            var configuration = builder.Build();

            services.AddDbContext<ApplicationContext>(
                options => options.UseNpgsql(configuration["DataSettings:ConnectionString"])
            );

            IContainer container = new Container(c =>
            {
                c.AddRegistry<MessageProcessingRegistry>();
                // c.For<IHttpContextAccessor>().Use(httpCtx);
            });
            container.Populate(services);

            _db = container.GetInstance<ApplicationContext>();
            _mediator = container.GetInstance<IMediator>();
        }

        [Fact]
        public async Task ItShouldReturnEmptyListWhenCompanyHaveNoDrivers()
        {
            Randomizer.Seed = new Random(3897234);
            GetCompanyDrivers.Request request = new Faker<GetCompanyDrivers.Request>()
                .RuleFor(c => c.CompanyId, f => Guid.NewGuid())
                .Generate();

            ICollection<GetCompanyDrivers.Response> actualCompanyDrivers = await _mediator.SendAsync(request).ConfigureAwait(false);

            actualCompanyDrivers.ShouldBeEmpty();
        }
        [Fact]
        public async Task ItShouldReturnDriversWhenCompanyHaveDrivers()
        {
            Randomizer.Seed = new Random(3897234);
            DataModels.CompanyContext.Company company = new Faker<DataModels.CompanyContext.Company>()
                    .RuleFor(c => c.Name, f => f.Company.CompanyName())
                    .Generate();

            DataModels.DriverContext.Driver driver = new Faker<DataModels.DriverContext.Driver>()
                .RuleFor(c => c.FirstName, f => f.Person.FirstName)
                .RuleFor(c => c.LastName, f => f.Person.LastName)
                .RuleFor(c => c.Email, f => f.Person.Email)
                .RuleFor(c => c.Company, f => company)
                .Generate();

            _db.Add(company);
            _db.Add(driver);
            await _db.SaveChangesAsync();

            GetCompanyDrivers.Request request = new GetCompanyDrivers.Request
            {
                CompanyId = company.Id
            };

            ICollection<GetCompanyDrivers.Response> actualCompanyDrivers = await _mediator.SendAsync(request).ConfigureAwait(false);

            actualCompanyDrivers.ShouldNotBeEmpty();
            actualCompanyDrivers.Count.ShouldBe(1);

            GetCompanyDrivers.Response companyDriver = actualCompanyDrivers.FirstOrDefault();
            companyDriver.Email.ShouldBe(driver.Email);
            companyDriver.FirstName.ShouldBe(driver.FirstName);
            companyDriver.LastName.ShouldBe(driver.LastName);
            companyDriver.Id.ShouldBe(driver.Id);
            companyDriver.Created.ShouldBe(driver.Created);
        }
    }
}
