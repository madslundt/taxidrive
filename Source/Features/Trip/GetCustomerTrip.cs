using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using DataModels.TripPositionContext;
using System.Collections.Generic;

namespace Source.Features.Trip
{
    public class GetCustomerTrip
    {
        public class Response
        {
            public GetPrice.Response Price { get; set; }
            public DateTime ETA { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Coordinate Origin { get; set; }
            public Coordinate Destination { get; set; }
        }

        public class Coordinate
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Origin).NotNull();
                RuleFor(query => query.Destination).NotNull();

                When(query => query.Origin != null, () =>
                {
                    RuleFor(query => query.Origin.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Origin.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
                When(query => query.Destination != null, () =>
                {
                    RuleFor(query => query.Destination.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Destination.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly IMediator _mediator;
            public Handler(IMediator mediator)
            {
                _mediator = mediator;
            }

            public async Task<Response> Handle(Request request)
            {
                GetPrice.Response price = await GetPrice(request).ConfigureAwait(false);

                DateTime eta = DateTime.UtcNow;

                Response result = new Response
                {
                    Price = price,
                    ETA = eta
                };

                return result;
            }

            private async Task<GetPrice.Response> GetPrice(Request request)
            {
                GetPrice.Coordinate origin = new GetPrice.Coordinate
                {
                    Longitude = request.Origin.Longitude,
                    Latitude = request.Origin.Latitude
                };
                GetPrice.Coordinate destination = new GetPrice.Coordinate
                {
                    Longitude = request.Destination.Longitude,
                    Latitude = request.Destination.Latitude
                };

                GetPrice.Request priceRequest = new GetPrice.Request
                {
                    Origin = origin,
                    Destination = destination
                };

                GetPrice.Response result = await _mediator.SendAsync(priceRequest).ConfigureAwait(false);

                return result;
            }
        }
    }
}
