using System;
using MediatR;
using FluentValidation;

namespace Source.Features.Trip
{
    public class RadiusService
    {
        public class Response
        {
            public Request MinimumPostion { get; set; }
            public Request MaximumPosition { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                RuleFor(query => query.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
            }
        }

        public class GetRadiuses : IGetRadius
        {
            private const int EARTH_RADIUS = 6371000;
            private const int RADIUS = 3000; // 3000 meter

            // http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
            // http://www.movable-type.co.uk/scripts/latlong-db.html
            public Response GetRadius(Request request)
            {
                Request minimumPosition = GetMinimumCoordinate(request);
                Request maximumPostiion = GetMaximumCoordinate(request);

                Response result = new Response
                {
                    MinimumPostion = minimumPosition,
                    MaximumPosition = maximumPostiion
                };

                return result;
            }

            private Request GetMinimumCoordinate(Request coordinate)
            {
                Request result = new Request
                {
                    Latitude = GetMinimumLatitude(coordinate.Latitude),
                    Longitude = GetMinimumLongitude(coordinate)
                };

                return result;
            }
            private Request GetMaximumCoordinate(Request coordinate)
            {
                Request result = new Request
                {
                    Latitude = GetMaximumLatitude(coordinate.Latitude),
                    Longitude = GetMaximumLongitude(coordinate)
                };

                return result;
            }

            private double GetMaximumLongitude(Request coordinate)
            {
                double radian = DegreeToRadian(coordinate.Latitude);
                double degree = RadianToDegree(Math.Asin(RADIUS / EARTH_RADIUS) / Math.Cos(radian));

                double result = coordinate.Longitude + degree;

                return result;
            }

            private double GetMinimumLongitude(Request coordinate)
            {
                double radian = DegreeToRadian(coordinate.Latitude);
                double degree = RadianToDegree(Math.Asin(RADIUS / EARTH_RADIUS) / Math.Cos(radian));

                double result = coordinate.Longitude - degree;

                return result;
            }

            private double GetMaximumLatitude(double latitude)
            {
                double degree = RadianToDegree(Math.Asin(RADIUS / EARTH_RADIUS));

                double result = latitude + degree;

                return result;
            }

            private double GetMinimumLatitude(double latitude)
            {
                double degree = RadianToDegree(Math.Asin(RADIUS / EARTH_RADIUS));

                double result = latitude - degree;

                return result;
            }

            private double RadianToDegree(double angle)
            {
                return angle * (180.0 / Math.PI);
            }
            private double DegreeToRadian(double angle)
            {
                return Math.PI * angle / 180.0;
            }
        }
    }
}
