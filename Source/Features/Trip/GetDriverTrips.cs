using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using DataModels.TripPositionContext;
using System.Collections.Generic;

namespace Source.Features.Trip
{
    public class GetDriverTrips
    {
        public class Response
        {
            public Guid Id { get; set; }
            public Position Origin { get; set; }
            public Position Destination { get; set; }
            public Guid CustomerId { get; set; }
            public ICollection<TripPosition> TripPositions { get; set; }
            public DateTime Created { get; set; }
        }
        public class Position
        {
            public double Altitude { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class TripPosition
        {
            public TripStatus TripStatus { get; set; }
            public DateTime Created { get; set; }
        }

        public class Request : IAsyncRequest<ICollection<Response>>
        {
            public Guid DriverId { get; set; }

            public int Limit { get; set; } = 10;
            public int Offset { get; set; } = 0;
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Limit).NotNull().GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
                RuleFor(query => query.Offset).NotNull().GreaterThanOrEqualTo(0);

                RuleFor(query => query.DriverId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, ICollection<Response>>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<ICollection<Response>> Handle(Request request)
            {
                ICollection<Response> result = await GetDriverTripQuery(request).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"Nothing found with that {nameof(request.DriverId)}");
                }

                return result;
            }

            private async Task<ICollection<Response>> GetDriverTripQuery(Request request)
            {
                var query = from trip in _db.Trips
                            join driver in _db.Drivers on trip.DriverId equals driver.Id
                            where driver.Id == request.DriverId

                            let tripPositions = (from tripPosition in _db.TripPositions
                                                 where tripPosition.TripId == trip.Id
                                                 select new TripPosition
                                                 {
                                                     TripStatus = tripPosition.Status,
                                                     Created = tripPosition.Created
                                                 }).ToList()

                            let origin = (from position in _db.Positions
                                          join coordinate in _db.Coordinates on position.CoordinateId equals coordinate.Id
                                          where position.Id == trip.OriginId
                                          select new Position
                                          {
                                                Altitude = coordinate.Altitude,
                                                Latitude = coordinate.Latitude,
                                                Longitude = coordinate.Longitude,
                                          }).FirstOrDefault()

                            let destination = (from position in _db.Positions
                                          join coordinate in _db.Coordinates on position.CoordinateId equals coordinate.Id
                                          where position.Id == trip.DestinationId
                                          select new Position
                                          {
                                                Altitude = coordinate.Altitude,
                                                Latitude = coordinate.Latitude,
                                                Longitude = coordinate.Longitude,
                                          }).FirstOrDefault()
                            select new Response
                            {
                                Id = trip.Id,
                                CustomerId = trip.CustomerId,
                                Origin = origin,
                                Destination = destination,
                                TripPositions = tripPositions,
                                Created = trip.Created
                            };

                ICollection<Response> result = await query
                    .Skip(request.Offset)
                    .Take(request.Limit)
                    .ToListAsync()
                    .ConfigureAwait(false);

                return result;
            }
        }
    }
}