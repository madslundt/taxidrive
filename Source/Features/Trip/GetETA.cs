using System;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;

namespace Source.Features.Trip
{
    public class GetETA
    {
        public class Response
        {
            public Guid DriverId { get; set; }
            public DateTime ETA { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Coordinate Origin { get; set; }
            public Coordinate Destination { get; set; }
        }

        public class Coordinate
        {
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Origin).NotNull();
                RuleFor(query => query.Destination).NotNull();

                When(query => query.Origin != null, () =>
                {
                    RuleFor(query => query.Origin.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Origin.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
                When(query => query.Destination != null, () =>
                {
                    RuleFor(query => query.Destination.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Destination.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            public Handler()
            {

            }

            public async Task<Response> Handle(Request request)
            {
                return null;
            }
        }
    }
}
