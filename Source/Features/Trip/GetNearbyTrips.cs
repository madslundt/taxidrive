using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using System.Collections.Generic;

namespace Source.Features.Trip
{
    public class GetNearbyTrips
    {
        public class Response
        {
            public Guid Id { get; set; }
            public Position Origin { get; set; }
            public Position Destination { get; set; }
            public Customer Customer { get; set; }
            public DateTime Created { get; set; }
        }
        public class Position
        {
            public double Altitude { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class Customer
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
        }

        public class Request : IAsyncRequest<ICollection<Response>>
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }

            public int Limit { get; set; } = 10;
            public int Offset { get; set; } = 0;
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Limit).NotNull().GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
                RuleFor(query => query.Offset).NotNull().GreaterThanOrEqualTo(0);

                RuleFor(query => query.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                RuleFor(query => query.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
            }
        }

        public class Handler : IAsyncRequestHandler<Request, ICollection<Response>>
        {
            private readonly ApplicationContext _db;
            private readonly IGetRadius _radius;

            public Handler(ApplicationContext db, IGetRadius radius)
            {
                _db = db;
                _radius = radius;
            }

            public async Task<ICollection<Response>> Handle(Request request)
            {
                ICollection<Response> result = await GetTrips(request).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException();
                }

                return result;
            }

            private async Task<ICollection<Response>> GetTrips(Request request)
            {
                RadiusService.Request radiusRequest = new RadiusService.Request
                {
                    Longitude = request.Longitude,
                    Latitude = request.Latitude
                };
                RadiusService.Response radius = _radius.GetRadius(radiusRequest);

                var query = from trip in _db.Trips
                            join coordinate in _db.Coordinates on trip.OriginId equals coordinate.Id
                            join customer in _db.Customers on trip.CustomerId equals customer.Id
                            where coordinate.Latitude >= radius.MinimumPostion.Latitude && coordinate.Latitude <= radius.MaximumPosition.Latitude
                            where coordinate.Longitude >= radius.MinimumPostion.Longitude && coordinate.Longitude <= radius.MaximumPosition.Longitude


                            let length = coordinate.Latitude + coordinate.Longitude

                            let destination = (from coordinate in _db.Coordinates
                                               where coordinate.Id == trip.DestinationId
                                               select new Position
                                               {
                                                    Altitude = coordinate.Altitude,
                                                    Latitude = coordinate.Latitude,
                                                    Longitude = coordinate.Longitude
                                               }).FirstOrDefault()

                            let tripStatus = (from tripPosition in _db.TripPositions
                                                        where tripPosition.TripId == trip.Id
                                                        select tripPosition.Status).ToList()

                            where tripStatus.Count() == 1 && tripStatus.Contains(DataModels.TripPositionContext.TripStatus.Requesting)
                            orderby length, trip.Created
                            select new Response
                            {
                                Id = trip.Id,
                                Origin = new Position
                                {
                                    Altitude = coordinate.Altitude,
                                    Latitude = coordinate.Latitude,
                                    Longitude = coordinate.Longitude
                                },
                                Destination = destination,
                                Customer = new Customer
                                {
                                    Id = customer.Id,
                                    FirstName = customer.FirstName,
                                    LastName = customer.LastName,
                                    Email = customer.Email
                                },
                                Created = trip.Created
                            };

                ICollection<Response> result = await query
                    .Skip(request.Offset)
                    .Take(request.Limit)
                    .ToListAsync()
                    .ConfigureAwait(false);

                return result;
            }
        }
    }
}