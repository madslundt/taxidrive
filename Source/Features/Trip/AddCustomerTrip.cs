using System;
using System.Threading.Tasks;
using FluentValidation;
using DataModels;
using MediatR;

namespace Source.Features.Trip
{
    public class AddCustomerTrip
    {
        public class Response
        {
            public Guid Id { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid CustomerId { get; set; }
            public Trip Trip { get; set; }
        }

        public class Trip
        {
            public Coordinate Origin { get; set; }
            public Coordinate Destination { get; set; }
        }

        public class Coordinate
        {
            public double Altitude { get; set; }
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.Trip).NotNull();

                When(query => query.Trip != null, () =>
                {
                    RuleFor(query => query.Trip.Origin).NotNull();
                    RuleFor(query => query.Trip.Destination).NotNull();

                    When(query => query.Trip.Origin != null, () =>
                    {
                        RuleFor(query => query.Trip.Origin.Altitude).NotNull().GreaterThanOrEqualTo(0);
                        RuleFor(query => query.Trip.Origin.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                        RuleFor(query => query.Trip.Origin.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                    });
                    When(query => query.Trip.Destination != null, () =>
                    {
                        RuleFor(query => query.Trip.Destination.Altitude).NotNull().GreaterThanOrEqualTo(0);
                        RuleFor(query => query.Trip.Destination.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                        RuleFor(query => query.Trip.Destination.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                    });
                });


                RuleFor(query => query.CustomerId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;
            // private readonly IMediator _mediator;

            public Handler(ApplicationContext db)
            {
                _db = db;
                // _mediator = mediator;
            }

            public async Task<Response> Handle(Request request)
            {
                // GetPrice.Response price = await _mediator.SendAsync(
                //     new GetPrice.Request
                //     {
                //         Origin = new GetPrice.Coordinate
                //         {
                //             Altitude = request.Trip.Origin.Altitude,
                //             Longitude = request.Trip.Origin.Longitude,
                //             Latitude = request.Trip.Origin.Latitude
                //         },
                //         Destination = new GetPrice.Coordinate
                //         {
                //             Altitude = request.Trip.Destination.Altitude,
                //             Longitude = request.Trip.Destination.Longitude,
                //             Latitude = request.Trip.Destination.Latitude
                //         }
                //     }).ConfigureAwait(false);

                DataModels.TripContext.Trip trip = AddTripQuery(request);

                // AddPriceToTrip(price, trip);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                Response result = new Response
                {
                    Id = trip.Id
                };

                return result;
            }

            private void AddPriceToTrip(GetPrice.Response price, DataModels.TripContext.Trip trip)
            {
                DataModels.PriceContext.Price p = new DataModels.PriceContext.Price
                {
                    Value = price.Price,
                    Currency = price.Currency
                };

                trip.Price = p;

                _db.Add(trip); // TODO could be removed?
            }

            private DataModels.TripContext.Trip AddTripQuery(Request request)
            {
                DataModels.CoordinateContext.Coordinate origin = new DataModels.CoordinateContext.Coordinate
                {
                    Altitude = request.Trip.Origin.Altitude,
                    Latitude = request.Trip.Origin.Latitude,
                    Longitude = request.Trip.Origin.Longitude
                };
                DataModels.CoordinateContext.Coordinate destination = new DataModels.CoordinateContext.Coordinate
                {
                    Altitude = request.Trip.Origin.Altitude,
                    Latitude = request.Trip.Origin.Latitude,
                    Longitude = request.Trip.Origin.Longitude
                };

                DataModels.TripContext.Trip trip = new DataModels.TripContext.Trip
                {
                    CustomerId = request.CustomerId,
                    Origin = origin,
                    Destination = destination
                };

                _db.Add(trip);

                return trip;
            }
        }
    }
}