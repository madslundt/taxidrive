using static Source.Features.Trip.RadiusService;

namespace Source.Features.Trip
{
    public interface IGetRadius
    {
        Response GetRadius(Request request);
    }
}