using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using DataModels;
using MediatR;
using DataModels.CoordinateContext;
using DataModels.TripPositionContext;
using DataModels.DriverTripPositionContext;
using DataModels.TripContext;
using Microsoft.EntityFrameworkCore;

namespace Source.Features.Trip
{
    public class AddDriverTripPosition
    {
        public class Response
        {
            public Guid Id { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid DriverId { get; set; }
            public Guid TripId { get; set; }
            public Position Position { get; set; }
        }

        public class Position
        {
            public double Accuracy { get; set; }
            public double AltitudeAccuracy { get; set; }
            public double Heading { get; set; }
            public double Speed { get; set; }

            public double Altitude { get; set; }
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.Position).NotNull();

                When(query => query.Position != null, () =>
                {
                    RuleFor(query => query.Position.Accuracy).NotNull();
                    RuleFor(query => query.Position.AltitudeAccuracy).NotNull();
                    RuleFor(query => query.Position.Heading).NotNull();
                    RuleFor(query => query.Position.Speed).NotNull().GreaterThanOrEqualTo(0);
                    RuleFor(query => query.Position.Altitude).NotNull();
                    RuleFor(query => query.Position.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Position.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });

                RuleFor(query => query.DriverId).NotEmpty();
                RuleFor(query => query.TripId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                Guid tripPositionId = await GetTrip(request.TripId, request.DriverId).ConfigureAwait(false);

                if (tripPositionId == null || tripPositionId == Guid.Empty)
                {
                    throw new ArgumentNullException($"No trip found for {nameof(request.TripId)} and {nameof(request.DriverId)}");
                }

                DriverTripPosition tripPosition = AddPositionQuery(request, tripPositionId);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                Response result = new Response
                {
                    Id = tripPosition.Id
                };

                return result;
            }

            private async Task<Guid> GetTrip(Guid tripId, Guid customerId)
            {
                var query = from trip in _db.Trips
                            join tripPosition in _db.TripPositions on trip.Id equals tripPosition.TripId
                            where trip.Id == tripId && trip.DriverId == customerId
                            where tripPosition.Status == TripStatus.Driving
                            select tripPosition.Id;

                Guid result = await query
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);

                return result;
            }

            private DriverTripPosition AddPositionQuery(Request request, Guid tripPositionId)
            {
                Coordinate coordinate = GetCoordinate(request.Position);
                DataModels.PositionContext.Position position = GetPosition(request, coordinate);

                DriverTripPosition tripPosition = GetTripPosition(tripPositionId, position, request.DriverId);

                _db.Add(tripPosition);

                return tripPosition;
            }

            private DriverTripPosition GetTripPosition(Guid tripPositionId, DataModels.PositionContext.Position position, Guid customerId)
            {
                DriverTripPosition result = new DriverTripPosition
                {
                    TripPositionId = tripPositionId,
                    Position = position,
                    DriverId = customerId
                };

                return result;
            }

            private DataModels.PositionContext.Position GetPosition(Request request, Coordinate coordinate)
            {
                DataModels.PositionContext.Position result = new DataModels.PositionContext.Position
                {
                    Accuracy = request.Position.Accuracy,
                    AltitudeAccuracy = request.Position.AltitudeAccuracy,
                    Heading = request.Position.Heading,
                    Speed = request.Position.Speed,
                    Coordinate = coordinate
                };

                return result;
            }

            private Coordinate GetCoordinate(Position position)
            {
                Coordinate result = new Coordinate
                {
                    Altitude = position.Altitude,
                    Latitude = position.Latitude,
                    Longitude = position.Longitude
                };

                return result;
            }
        }
    }
}