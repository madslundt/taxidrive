using System;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using DataModels.PriceContext;
using Source.Features.Google;

namespace Source.Features.Trip
{
    public class GetPrice
    {
        public class Response
        {
            public decimal Price { get; set; }
            public Currency Currency { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Coordinate Origin { get; set; }
            public Coordinate Destination { get; set; }
        }

        public class Coordinate
        {
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Origin).NotNull();
                RuleFor(query => query.Destination).NotNull();

                When(query => query.Origin != null, () =>
                {
                    RuleFor(query => query.Origin.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Origin.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
                When(query => query.Destination != null, () =>
                {
                    RuleFor(query => query.Destination.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Destination.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly IGetDirections _googleDirections;
            public Handler(IGetDirections getDirections)
            {
                _googleDirections = getDirections;
            }

            public async Task<Response> Handle(Request request)
            {
                Response result = await CalculatePrice(request);

                if (result == null)
                {
                    throw new ArgumentNullException($"Could not calculate price between {nameof(request.Origin)} and {nameof(request.Destination)}");
                }

                return result;
            }

            private async Task<Response> CalculatePrice(Request request)
            {
                GoogleServie.Request googleDiretionsRequest = new GoogleServie.Request
                {
                    Origin = new GoogleServie.Coordinate
                    {
                        Latitude = request.Origin.Latitude,
                        Longitude = request.Origin.Longitude
                    },
                    Destination = new GoogleServie.Coordinate
                    {
                        Latitude = request.Destination.Latitude,
                        Longitude = request.Destination.Longitude
                    }
                };

                var googleDiretionsResponse = await _googleDirections.GetDirection(googleDiretionsRequest);

                return null;
            }
        }
    }
}
