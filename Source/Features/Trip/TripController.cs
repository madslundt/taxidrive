using System;
using Microsoft.AspNetCore.Mvc;
using DataModels.TripPositionContext;
using System.Threading.Tasks;
using MediatR;

namespace Source.Features.Trip
{
    public class TripController : Controller
    {
        private readonly IMediator _mediator;

        public TripController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet, Route("api/trips/{tripId}")]
        public async Task<IActionResult> GetTrip(Guid tripId)
        {
            var result = await _mediator.SendAsync(
                new GetTrip.Request
                {
                    TripId = tripId
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpGet, Route("api/drivers/{driverId}/trips")]
        public async Task<IActionResult> GetDriverTrips(Guid driverId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetDriverTrips.Request
                {
                    DriverId = driverId,
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpGet, Route("api/customers/{customerId}/trips")]
        public async Task<IActionResult> GetCustomerTrips(Guid customerId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetCustomerTrips.Request
                {
                    CustomerId = customerId,
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpPost, Route("api/customers/trips")]
        public async Task<IActionResult> CustomerRequestTrip([FromBody] AddCustomerTrip.Trip trip)
        {
            var result = await _mediator.SendAsync(
                new AddCustomerTrip.Request
                {
                    CustomerId = Guid.NewGuid(), // TODO
                    Trip = trip
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpGet, Route("api/customers/trips")]
        public async Task<IActionResult> DriverAddTrip([FromQuery] double originLatitude, [FromQuery] double originLongitude, [FromQuery] double destinationLatitude, [FromQuery] double destinationLongitude)
        {
            GetETA.Coordinate origin = new GetETA.Coordinate
            {
                Latitude = originLatitude,
                Longitude = originLongitude
            };
            GetETA.Coordinate destination = new GetETA.Coordinate
            {
                Latitude = destinationLatitude,
                Longitude = destinationLongitude
            };

            var result = await _mediator.SendAsync(
                new GetETA.Request
                {
                    Origin = origin,
                    Destination = destination
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpGet, Route("api/drivers/trips")]
        public async Task<IActionResult> DriverGetNearbyTrips([FromQuery] double latitude, [FromQuery] double longitude, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetNearbyTrips.Request
                {
                    Latitude = latitude,
                    Longitude = longitude,
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpPut, Route("api/drivers/trips/{tripId}")]
        public async Task<IActionResult> DriverAddTrip(Guid tripId) // TODO
        {
            var result = await _mediator.SendAsync(
                new AddDriverTrip.Request
                {
                    DriverId = Guid.NewGuid(), // TODO
                    TripId = tripId
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpPut, Route("api/customers/trips/{tripId}/statuses")]
        public async Task<IActionResult> CustomerChangeTripStatus(Guid tripId, [FromQuery] TripStatus status)
        {
            var result = await _mediator.SendAsync(
                new ChangeCustomerTripStatus.Request
                {
                    CustomerId = Guid.NewGuid(), // TODO
                    TripId = tripId,
                    Status = status
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpPut, Route("api/drivers/trips/{tripId}/statuses")]
        public async Task<IActionResult> DriverChangeTripStatus(Guid tripId, [FromQuery] TripStatus status)
        {
            var result = await _mediator.SendAsync(
                new ChangeDriverTripStatus.Request
                {
                    DriverId = Guid.NewGuid(), // TODO
                    TripId = tripId,
                    Status = status
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpPost, Route("api/customers/trips/{tripId}/positions")]
        public async Task<IActionResult> AddCustomerTripPosition(Guid tripId, [FromBody] AddCustomerTripPosition.Position position) // TODO
        {
            var result = await _mediator.SendAsync(
                new AddCustomerTripPosition.Request
                {
                    CustomerId = Guid.NewGuid(), // TODO
                    TripId = tripId,
                    Position = position
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpPost, Route("api/drivers/trips/{tripId}/positions")]
        public async Task<IActionResult> AddDriverTripPosition(Guid tripId, [FromBody] AddDriverTripPosition.Position position) // TODO
        {
            var result = await _mediator.SendAsync(
                new AddDriverTripPosition.Request
                {
                    DriverId = Guid.NewGuid(), // TODO
                    TripId = tripId,
                    Position = position
                }).ConfigureAwait(false);

            return Ok(result);
        }
    }
}
