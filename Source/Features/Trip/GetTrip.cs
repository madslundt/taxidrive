using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using DataModels.TripPositionContext;
using System.Collections.Generic;

namespace Source.Features.Trip
{
    public class GetTrip
    {
        public class Response
        {
            public Guid Id { get; set; }
            public Position Origin { get; set; }
            public Position Destination { get; set; }
            public Guid CustomerId { get; set; }
            public Guid DriverId { get; set; }
            public ICollection<TripPosition> TripPositions { get; set; }
            public DateTime Created { get; set; }
        }
        public class Position
        {
            public double Altitude { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class TripPosition
        {
            public TripStatus TripStatus { get; set; }
            public DateTime Created { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid TripId { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.TripId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                Response result = await GetTripQuery(request.TripId).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"Nothing found with that {nameof(request.TripId)}");
                }

                return result;
            }

            private async Task<Response> GetTripQuery(Guid tripId)
            {
                var query = from trip in _db.Trips
                            where trip.Id == tripId

                            let tripPositions = (from tripPosition in _db.TripPositions
                                                 where tripPosition.TripId == trip.Id
                                                 orderby tripPosition.Created
                                                 select new TripPosition
                                                 {
                                                     TripStatus = tripPosition.Status,
                                                     Created = tripPosition.Created
                                                 }).ToList()

                            let origin = (from position in _db.Positions
                                          join coordinate in _db.Coordinates on position.CoordinateId equals coordinate.Id
                                          where position.Id == trip.OriginId
                                          select new Position
                                          {
                                                Altitude = coordinate.Altitude,
                                                Latitude = coordinate.Latitude,
                                                Longitude = coordinate.Longitude,
                                          }).FirstOrDefault()

                            let destination = (from position in _db.Positions
                                          join coordinate in _db.Coordinates on position.CoordinateId equals coordinate.Id
                                          where position.Id == trip.DestinationId
                                          select new Position
                                          {
                                                Altitude = coordinate.Altitude,
                                                Latitude = coordinate.Latitude,
                                                Longitude = coordinate.Longitude,
                                          }).FirstOrDefault()
                            select new Response
                            {
                                Id = trip.Id,
                                CustomerId = trip.CustomerId,
                                DriverId = trip.DriverId,
                                Origin = origin,
                                Destination = destination,
                                TripPositions = tripPositions,
                                Created = trip.Created
                            };

                Response result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }
        }
    }
}
