using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using DataModels.CoordinateContext;
using DataModels.TripPositionContext;

namespace Source.Features.Trip
{
    public class ChangeDriverTripStatus
    {
        public class Response
        {
            public Guid Id { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid TripId { get; set; }
            public Guid DriverId { get; set; }
            public TripStatus Status { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Status).Must(ValidStatus);

                RuleFor(query => query.TripId).NotEmpty();
                RuleFor(query => query.DriverId).NotEmpty();
            }

            private bool ValidStatus(TripStatus status)
            {
                return status == TripStatus.DriverCancelled || status == TripStatus.Ready
                        || status == TripStatus.Driving || status == TripStatus.Done; // TODO
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                bool hasDriverAccessToTrip = await HasDriverAccessToTripQuery(request.DriverId, request.TripId).ConfigureAwait(false);

                if (!hasDriverAccessToTrip)
                {
                    throw new MemberAccessException(); // TODO
                }

                TripPosition tripPosition = AddTripPositionQuery(request);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                Response result = new Response
                {
                    Id = tripPosition.Id
                };

                return result;
            }

            private async Task<bool> HasDriverAccessToTripQuery(Guid driverId, Guid tripId)
            {
                var query = from trip in _db.Trips
                            where trip.DriverId == driverId && trip.Id == tripId
                            select trip.Id;

                bool result = await query.AnyAsync().ConfigureAwait(false);

                return result;
            }

            private TripPosition AddTripPositionQuery(Request request)
            {
                TripPosition tripPosition = new TripPosition
                {
                    TripId = request.TripId,
                    Status = request.Status
                };

                _db.Add(tripPosition);

                return tripPosition;
            }
        }
    }
}