using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using DataModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using DataModels.TripPositionContext;
using System.Collections.Generic;

namespace Source.Features.Trip
{
    public class AddDriverTrip
    {
        public class Response
        {
            public Guid Id { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid DriverId { get; set; }
            public Guid TripId { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.DriverId).NotEmpty();
                RuleFor(query => query.TripId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                bool isTripValid = await IsTripValidQuery(request.TripId).ConfigureAwait(false);

                if (!isTripValid)
                {
                    throw new ArgumentNullException($"Could not add {nameof(request.DriverId)} to {nameof(request.TripId)}");
                }

                TripPosition tripPosition = AddTripPositionQuery(request.TripId);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                Response result = new Response
                {
                    Id = tripPosition.Id
                };

                return result;
            }

            private async Task<bool> IsTripValidQuery(Guid tripId)
            {
                var query = from trip in _db.Trips
                            join tripPosition in _db.TripPositions on trip.Id equals tripPosition.TripId
                            where trip.Id == tripId && trip.DriverId == Guid.Empty
                            select tripPosition.Status;

                ICollection<DataModels.TripPositionContext.TripStatus> statuses = await query.ToListAsync().ConfigureAwait(false);

                return statuses.Count == 1 && statuses.First() == DataModels.TripPositionContext.TripStatus.Requesting;
            }

            private TripPosition AddTripPositionQuery(Guid tripId)
            {
                TripPosition tripPosition = new TripPosition
                {
                    TripId = tripId,
                    Status = TripStatus.Accepted
                };

                _db.Add(tripPosition);

                return tripPosition;
            }
        }
    }
}