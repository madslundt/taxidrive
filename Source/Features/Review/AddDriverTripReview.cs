using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using DataModels.DriverToCustomerReviewContext;

namespace Source.Features.Review
{
    public class AddDriverTripReview
    {
        public class Response
        {
            public Guid Id { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid TripId { get; set; }
            public Guid DriverId { get; set; }

            public Review Review { get; set; }
        }

        public class Review
        {
            public int Rating { get; set; }
            public string Text { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.Review).NotNull();

                When(query => query.Review != null, () =>
                {
                    RuleFor(query => query.Review.Rating).GreaterThanOrEqualTo(1).LessThanOrEqualTo(5); // TODO
                });

                RuleFor(query => query.TripId).NotEmpty();
                RuleFor(query => query.DriverId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                Guid customerId = await GetCustomerIdFromTripAndDriverQuery(
                    request.TripId, request.DriverId)
                    .ConfigureAwait(false);

                if (customerId == null || customerId == Guid.Empty)
                {
                    throw new MemberAccessException(); // TODO better exception
                }

                DriverToCustomerReview review = AddDriverReviewQuery(
                    request, customerId);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                Response result = new Response
                {
                    Id = review.Id
                };

                return result;
            }

            private async Task<Guid> GetCustomerIdFromTripAndDriverQuery(Guid tripId, Guid driverId)
            {
                var query = from trip in _db.Trips
                            where trip.Id == tripId && trip.DriverId == driverId
                            select trip.DriverId;

                Guid result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }

            private DriverToCustomerReview AddDriverReviewQuery(Request request, Guid customerId)
            {
                DriverToCustomerReview review = new DriverToCustomerReview
                {
                    TripId = request.TripId,
                    CreatorId = request.DriverId,
                    CustomerId = customerId,
                    Rating = request.Review.Rating,
                    Text = request.Review.Text
                };

                _db.Add(review);

                return review;
            }
        }
    }
}