using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using DataModels.CustomerToDriverReviewContext;

namespace Source.Features.Review
{
    public class AddCustomerTripReview
    {
        public class Response
        {
            public Guid Id { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid TripId { get; set; }
            public Guid CustomerId { get; set; }

            public Review Review { get; set; }
        }

        public class Review
        {
            public int Rating { get; set; }
            public string Text { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.Review).NotNull();

                When(query => query.Review != null, () =>
                {
                    RuleFor(query => query.Review.Rating).GreaterThanOrEqualTo(1).LessThanOrEqualTo(5); // TODO
                });

                RuleFor(query => query.TripId).NotEmpty();
                RuleFor(query => query.CustomerId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                Guid driverId = await GetDriverIdFromTripAndCustomerQuery(
                    request.TripId, request.CustomerId)
                    .ConfigureAwait(false);

                if (driverId == null || driverId == Guid.Empty)
                {
                    throw new MemberAccessException(); // TODO better exception
                }

                CustomerToDriverReview review = AddDriverReviewQuery(
                    request, driverId);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                Response result = new Response
                {
                    Id = review.Id
                };

                return result;
            }

            private async Task<Guid> GetDriverIdFromTripAndCustomerQuery(Guid tripId, Guid customerId)
            {
                var query = from trip in _db.Trips
                            where trip.Id == tripId && trip.CustomerId == customerId
                            select trip.DriverId;

                Guid result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }

            private CustomerToDriverReview AddDriverReviewQuery(Request request,  Guid driverId)
            {
                CustomerToDriverReview review = new CustomerToDriverReview
                {
                    TripId = request.TripId,
                    CreatorId = request.CustomerId,
                    DriverId = driverId,
                    Rating = request.Review.Rating,
                    Text = request.Review.Text
                };

                _db.Add(review);

                return review;
            }
        }
    }
}