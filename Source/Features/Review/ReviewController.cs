using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MediatR;

namespace Source.Features.Review
{
    public class ReviewController : Controller
    {
        private readonly IMediator _mediator;

        public ReviewController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet, Route("api/drivers/{driverId}/reviews")]
        public async Task<IActionResult> GetDriverReviews(Guid driverId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetDriverReviews.Request
                {
                    DriverId = driverId,
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpGet, Route("api/customers/{customerId}/reviews")]
        public async Task<IActionResult> GetCustomerReviews(Guid customerId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetCustomerReviews.Request
                {
                    CustomerId = customerId,
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpGet, Route("api/companies/{companyId}/reviews")]
        public async Task<IActionResult> GetCompanyReviews(Guid companyId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetCompanyReviews.Request
                {
                    CompanyId = companyId,
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpGet, Route("api/customers/drivers/{driverId}/reviews")]
        public async Task<IActionResult> GetCustomerReviewsToDriver(Guid driverId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetCustomerToDriverReviews.Request
                {
                    DriverId = driverId,
                    CustomerId = Guid.Empty, /* TODO current user */
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpGet, Route("api/drivers/customers/{customerId}/reviews")]
        public async Task<IActionResult> GetDriverReviewsToCustomer(Guid customerId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetDriverToCustomerReviews.Request
                {
                    CustomerId = customerId,
                    DriverId = Guid.Empty, /* TODO current user */
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpPost, Route("api/customers/trips/{tripId}/reviews")]
        public async Task<IActionResult> AddCustomerTripReview(Guid tripId, [FromBody] AddCustomerTripReview.Review review)
        {
            var result = await _mediator.SendAsync(
                new AddCustomerTripReview.Request
                {
                    CustomerId = Guid.Empty, /* TODO current user */
                    TripId = tripId,
                    Review = review
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpPost, Route("api/drivers/trips/{tripId}/reviews")]
        public async Task<IActionResult> AddDriverTripReview(Guid tripId, [FromBody] AddDriverTripReview.Review review)
        {
            var result = await _mediator.SendAsync(
                new AddDriverTripReview.Request
                {
                    DriverId = Guid.Empty, /* TODO current user */
                    TripId = tripId,
                    Review = review
                }).ConfigureAwait(false);

            return Ok(result);
        }
    }
}
