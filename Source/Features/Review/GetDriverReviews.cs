using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using System.Collections.Generic;
using MediatR;

namespace Source.Features.Review
{
    public class GetDriverReviews
    {
        public class Response
        {
            public Guid Id { get; set; }
            public Guid TripId { get; set; }
            public Guid CustomerId { get; set; }
            public int Rating { get; set; }
            public string Text { get; set; }
            public DateTime Created { get; set; }
        }

        public class Request : IAsyncRequest<ICollection<Response>>
        {
            public Guid DriverId { get; set; }
            public int Limit { get; set; } = 10;
            public int Offset { get; set; } = 0;
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Limit).NotNull().GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
                RuleFor(query => query.Offset).NotNull().GreaterThanOrEqualTo(0);

                RuleFor(query => query.DriverId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, ICollection<Response>>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<ICollection<Response>> Handle(Request request)
            {
                ICollection<Response> result = await GetDriverReviewsQuery(request)
                    .ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"Nothing found with that {nameof(request.DriverId)}");
                }

                return result;
            }

            private async Task<ICollection<Response>> GetDriverReviewsQuery(Request request)
            {
                var query = from review in _db.CustomerToDriverReviews
                            where review.DriverId == request.DriverId
                            select new Response
                            {
                                Id = review.Id,
                                TripId = review.TripId,
                                CustomerId = review.CreatorId,
                                Rating = review.Rating,
                                Text = review.Text,
                                Created = review.Created
                            };

                ICollection<Response> result = await query
                    .Skip(request.Offset)
                    .Take(request.Limit)
                    .ToListAsync()
                    .ConfigureAwait(false);

                return result;
            }
        }
    }
}
