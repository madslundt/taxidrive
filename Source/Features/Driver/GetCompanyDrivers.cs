using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;
using System.Collections.Generic;

namespace Source.Features.Driver
{
    public class GetCompanyDrivers
    {
        public class Response
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public DateTime Created { get; set; }
        }

        public class Request : IAsyncRequest<ICollection<Response>>
        {
            public Guid CompanyId { get; set; }
            public int Limit { get; set; } = 10;
            public int Offset { get; set; } = 0;
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Limit).NotNull().GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
                RuleFor(query => query.Offset).NotNull().GreaterThanOrEqualTo(0);

                RuleFor(query => query.CompanyId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, ICollection<Response>>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<ICollection<Response>> Handle(Request request)
            {
                ICollection<Response> result = await GetDriversQuery(request).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"No drivers found with that {nameof(request.CompanyId)}");
                }

                return result;
            }

            private async Task<ICollection<Response>> GetDriversQuery(Request request)
            {
                var query = from driver in _db.Drivers
                            join company in _db.Companies on driver.CompanyId equals company.Id
                            where company.Id == request.CompanyId
                            select new Response
                            {
                                Id = driver.Id,
                                FirstName = driver.FirstName,
                                LastName = driver.LastName,
                                Email = driver.Email,
                                Created = driver.Created
                            };

                ICollection<Response> result = await query
                    .Skip(request.Offset)
                    .Take(request.Limit)
                    .ToListAsync()
                    .ConfigureAwait(false);

                return result;
            }
        }
    }
}
