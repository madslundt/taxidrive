using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;

namespace Source.Features.Driver
{
    public class GetDriver
    {
        public class Response
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public Company Company { get; set; }
            public DateTime Created { get; set; }
        }

        public class Company
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid DriverId { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.DriverId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                Response result = await GetDriverQuery(request.DriverId).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"Nothing found with that {nameof(request.DriverId)}");
                }

                return result;
            }

            private async Task<Response> GetDriverQuery(Guid driverId)
            {
                var query = from driver in _db.Drivers
                            join company in _db.Companies on driver.CompanyId equals company.Id
                            where driver.Id == driverId
                            select new Response
                            {
                                Id = driver.Id,
                                FirstName = driver.FirstName,
                                LastName = driver.LastName,
                                Email = driver.Email,
                                Company = new Company
                                {
                                    Id = company.Id,
                                    Name = company.Name
                                },
                                Created = driver.Created
                            };

                Response result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }
        }
    }
}
