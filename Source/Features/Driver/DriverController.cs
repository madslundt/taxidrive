using System;
using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Source.Authentication.Signup;
using Source.Authentication.Signin;
using Microsoft.AspNetCore.Authorization;

namespace Source.Features.Driver
{
    public class DriverController : Controller
    {
        private readonly IMediator _mediator;

        public DriverController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet, Route("api/drivers/{driverId}")]
        public async Task<IActionResult> GetDriver(Guid driverId)
        {
            var result = await _mediator.SendAsync(
                new GetDriver.Request
                {
                    DriverId = driverId
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpGet, Route("api/companies/{companyId}/drivers")]
        public async Task<IActionResult> GetCompanyDrivers(Guid companyId, [FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            var result = await _mediator.SendAsync(
                new GetCompanyDrivers.Request
                {
                    CompanyId = companyId,
                    Limit = limit,
                    Offset = offset
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpPost, AllowAnonymous, Route("api/signup/drivers")]
        public async Task<IActionResult> CreateDriver([FromBody] SignupDriver.Request driver)
        {
            var result = await _mediator.SendAsync(
                new SignupDriver.Request
                {
                    Email = driver.Email,
                    Password = driver.Password,
                    FirstName = driver.FirstName,
                    LastName = driver.LastName
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpPost, AllowAnonymous, Route("api/signin/drivers")]
        public async Task<IActionResult> SigninDriver([FromBody] SigninDriver.Request driver)
        {
            var result = await _mediator.SendAsync(
                new SigninDriver.Request
                {
                    Email = driver.Email,
                    Password = driver.Password
                }).ConfigureAwait(false);

            return Ok(result);
        }

        // [HttpPost, Route("api/customers/drivers")]
        // public async Task<IActionResult> GetNearbyDrivers([FromBody] object request, [FromQuery] int limit = 10, [FromQuery] int offset = 0) // TODO
        // {
        //     throw new NotImplementedException();
        // }

        // [HttpPost, Route("api/companies/{companyId}/drivers")]
        // public async Task<IActionResult> addDriverToCompany(Guid companyId, [FromBody] object driver) // TODO
        // {
        //     throw new NotImplementedException();
        // }
    }
}
