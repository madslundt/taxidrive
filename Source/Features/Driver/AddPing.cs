using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using DataModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using DataModels.TripPositionContext;
using System.Collections.Generic;

namespace Source.Features.Driver
{
    public class AddPing
    {
        public class Response
        {

        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid DriverId { get; set; }
            public Coordinate Position { get; set; }
        }
        public class Coordinate
        {
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Position).NotNull();

                When(query => query.Position != null, () => {
                    RuleFor(query => query.Position.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Position.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            // private readonly ApplicationContext _db;

            public Handler()
            {
            }

            public async Task<Response> Handle(Request request)
            {
                return null;
            }
        }
    }
}