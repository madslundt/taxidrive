using System;
using System.Threading.Tasks;
using MediatR;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;
using FluentValidation;

namespace Source.Features.Google
{
    public class GoogleServie
    {
        public class Response
        {
            public double Duration { get; set; }
            public double Distance { get; set; }
        }
        private class GoogleDirectionsResponse
        {
            public List<GeocodedWaypoint> geocoded_waypoints { get; set; }
            public List<Route> routes { get; set; }
            public string status { get; set; }
        }

        private class GeocodedWaypoint
        {
            public string geocoder_status { get; set; }
            public string place_id { get; set; }
            public List<string> types { get; set; }
        }

        private class Northeast
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        private class Southwest
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        private class Bounds
        {
            public Northeast northeast { get; set; }
            public Southwest southwest { get; set; }
        }

        private class Distance
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        private class Duration
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        private class EndLocation
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        private class StartLocation
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        private class Distance2
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        private class Duration2
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        private class EndLocation2
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        private class Polyline
        {
            public string points { get; set; }
        }

        private class StartLocation2
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        private class Step
        {
            public Distance2 distance { get; set; }
            public Duration2 duration { get; set; }
            public EndLocation2 end_location { get; set; }
            public string html_instructions { get; set; }
            public Polyline polyline { get; set; }
            public StartLocation2 start_location { get; set; }
            public string travel_mode { get; set; }
            public string maneuver { get; set; }
        }

        private class Leg
        {
            public Distance distance { get; set; }
            public Duration duration { get; set; }
            public string end_address { get; set; }
            public EndLocation end_location { get; set; }
            public string start_address { get; set; }
            public StartLocation start_location { get; set; }
            public List<Step> steps { get; set; }
            public List<object> traffic_speed_entry { get; set; }
            public List<object> via_waypoint { get; set; }
        }

        private class OverviewPolyline
        {
            public string points { get; set; }
        }

        private class Route
        {
            public Bounds bounds { get; set; }
            public string copyrights { get; set; }
            public List<Leg> legs { get; set; }
            public OverviewPolyline overview_polyline { get; set; }
            public string summary { get; set; }
            public List<object> warnings { get; set; }
            public List<object> waypoint_order { get; set; }
        }


        public class Request : IAsyncRequest<Response>
        {
            public Coordinate Origin { get; set; }
            public Coordinate Destination { get; set; }
        }
        public class Coordinate
        {
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();

                RuleFor(query => query.Origin).NotNull();
                RuleFor(query => query.Destination).NotNull();

                When(query => query.Origin != null, () =>
                {
                    RuleFor(query => query.Origin.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Origin.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
                When(query => query.Destination != null, () =>
                {
                    RuleFor(query => query.Destination.Longitude).NotNull().GreaterThanOrEqualTo(-180).LessThanOrEqualTo(180);
                    RuleFor(query => query.Destination.Latitude).NotNull().GreaterThanOrEqualTo(-90).LessThanOrEqualTo(90);
                });
            }
        }

        public class GetDirections : IGetDirections
        {
            public async Task<Response> GetDirection(Request request)
            {
                Response result = await GetDirectionsQuery(request).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"No results between {nameof(request.Origin)} and {nameof(request.Destination)}");
                }

                return result;
            }

            private async Task<Response> GetDirectionsQuery(Request request)
            {
                string origin = CoordinateToString(request.Origin);
                string destination = CoordinateToString(request.Destination);

                const string apiKey = "AIzaSyCCzfa0rvJ-oGb8WvxO7ydqnrVtI_wisv8";

                string url = $"https://maps.googleapis.com/maps/api/directions/json?origin={origin}&destination={destination}&key={apiKey}";

                using (var httpClient = new HttpClient())
                {
                    var json = await httpClient.GetStringAsync(url);
                    var result = JsonConvert.DeserializeObject<Response>(json);

                    return result;
                }
            }


            private string CoordinateToString(Coordinate coordinate)
            {
                string result = $"{coordinate.Latitude},{coordinate.Longitude}";

                return result;
            }
        }
    }
}
