using System.Threading.Tasks;
using static Source.Features.Google.GoogleServie;

namespace Source.Features.Google
{
    public interface IGetDirections
    {
         Task<Response> GetDirection(Request request);
    }
}