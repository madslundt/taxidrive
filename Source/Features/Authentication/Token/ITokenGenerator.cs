﻿using System;
using static Source.Features.Authentication.Token.JwtTokenGenerator;

namespace Source.Features.Authentication.Token
{
    public interface ITokenGenerator
    {
        TokenResult Generate(Guid id, TokenType type);
    }
}