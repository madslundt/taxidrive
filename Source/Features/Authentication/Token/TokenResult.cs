﻿namespace Source.Features.Authentication.Token
{
    public class TokenResult
    {
        public string TokenType { get; set; }
        public string Token { get; set; }
        public string Expiration { get; set; }
    }
}