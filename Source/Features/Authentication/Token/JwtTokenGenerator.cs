﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Source.Infrastructure.Token;

namespace Source.Features.Authentication.Token
{
    public class JwtTokenGenerator : ITokenGenerator
    {
        public enum TokenType
        {
            Customer = 1,
            Driver = 2
        }
        private readonly TokenAuthOptions _options;

        public JwtTokenGenerator(TokenAuthOptions options)
        {
            _options = options;
        }

        public TokenResult Generate(Guid id, TokenType type)
        {
            const string tokenType = "Bearer";
            string expiration = DateTime.UtcNow.AddDays(30).ToString("O");
            var handler = new JwtSecurityTokenHandler();

            ClaimsIdentity identity = new ClaimsIdentity();
            identity.AddClaim(new Claim("Id", id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Expiration, expiration));
            identity.AddClaim(new Claim("TokenType", tokenType));

            JwtSecurityToken securityToken = handler.CreateJwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                signingCredentials: _options.SigningCredentials,
                subject: identity,
                expires: DateTime.Parse(expiration)
            );

            string token = handler.WriteToken(securityToken);

            TokenResult result = new TokenResult
            {
                TokenType = tokenType,
                Token = token,
                Expiration = expiration
            };

            return result;
        }
    }
}