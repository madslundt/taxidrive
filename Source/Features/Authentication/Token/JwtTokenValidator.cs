﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Source.Infrastructure.Token;
using Microsoft.IdentityModel.Tokens;

namespace Source.Features.Authentication.Token
{
    public class JwtTokenValidator : ITokenValidator
    {
        private readonly TokenAuthOptions _options;

        public JwtTokenValidator(TokenAuthOptions options)
        {
            _options = options;
        }

        public ClaimsPrincipal ValidateToken(string accessToken)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

            TokenValidationParameters validationParameters = new TokenValidationParameters()
            {
                ValidAudience = _options.Audience,
                ValidIssuer = _options.Issuer,
                IssuerSigningKey = _options.Key,
                ValidateIssuer = false
            };
            ClaimsPrincipal claims;
            try
            {
                SecurityToken validToken;
                claims = handler.ValidateToken(accessToken, validationParameters, out validToken);
            }
            catch (Exception)
            {
                throw;
            }

            return claims;
        }
    }
}