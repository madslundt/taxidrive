﻿using System.Security.Claims;

namespace Source.Features.Authentication.Token
{
    public interface ITokenValidator
    {
        ClaimsPrincipal ValidateToken(string accessToken);
    }
}