using System;
using System.Linq;
using System.Threading.Tasks;
using DataModels;
using DataModels.DriverContext;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Source.Features.Authentication.Token;
using Source.Features.Driver;

namespace Source.Authentication.Signin
{
    public class SigninDriver
    {
        public class Response
        {
            public GetDriver.Response Driver { get; set; }
            public TokenResult Token { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(q => q).NotNull();
                RuleFor(q => q.Email).NotEmpty().EmailAddress();
                RuleFor(q => q.Password.Length).GreaterThanOrEqualTo(6);
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {

            private readonly IPasswordHasher<Driver> _passwordHasher;
            private readonly ApplicationContext _db;
            private readonly ITokenGenerator _tokenGenerator;
            private readonly IMediator _mediator;

            public Handler(ApplicationContext db, IPasswordHasher<Driver> passwordHasher, ITokenGenerator token, IMediator mediator)
            {
                _tokenGenerator = token;
                _passwordHasher = passwordHasher;
                _db = db;
                _mediator = mediator;
            }

            public async Task<Response> Handle(Request message)
            {
                Driver driver = await GetDriver(message.Email).ConfigureAwait(false);

                if (driver == null)
                {
                    throw new ArgumentNullException(nameof(driver));
                }

                PasswordVerificationResult passwordResult = _passwordHasher.VerifyHashedPassword(driver, driver.Password, message.Password);
                if (passwordResult != PasswordVerificationResult.Success)
                {
                    throw new Exception($"{nameof(driver.Email)} and {nameof(driver.Password)} does not match");
                }

                TokenResult token;
                token = _tokenGenerator.Generate(driver.Id, JwtTokenGenerator.TokenType.Driver);

                GetDriver.Response driverResponse = await _mediator.SendAsync(new GetDriver.Request
                {
                    DriverId = driver.Id
                });

                Response result = new Response
                {
                    Driver = driverResponse,
                    Token = token
                };

                return result;
            }

            private async Task<Driver> GetDriver(string email)
            {
                var query = from driver in _db.Drivers
                            where driver.Email == email
                            select driver;


                Driver result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }
        }
    }
}