using System;
using System.Linq;
using System.Threading.Tasks;
using DataModels;
using DataModels.CustomerContext;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Source.Features.Authentication.Token;
using Source.Features.Customer;

namespace Source.Authentication.Signin
{
    public class SigninCustomer
    {
        public class Response
        {
            public GetCustomer.Response Customer { get; set; }
            public TokenResult Token { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(q => q).NotNull();
                RuleFor(q => q.Email).NotEmpty().EmailAddress();
                RuleFor(q => q.Password.Length).GreaterThanOrEqualTo(6);
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {

            private readonly IPasswordHasher<Customer> _passwordHasher;
            private readonly ApplicationContext _db;
            private readonly ITokenGenerator _tokenGenerator;
            private readonly IMediator _mediator;

            public Handler(ApplicationContext db, IPasswordHasher<Customer> passwordHasher, ITokenGenerator token, IMediator mediator)
            {
                _tokenGenerator = token;
                _passwordHasher = passwordHasher;
                _db = db;
                _mediator = mediator;
            }

            public async Task<Response> Handle(Request message)
            {
                Customer customer = await GetCustomer(message.Email).ConfigureAwait(false);

                if (customer == null)
                {
                    throw new ArgumentNullException(nameof(customer));
                }

                PasswordVerificationResult passwordResult = _passwordHasher.VerifyHashedPassword(customer, customer.Password, message.Password);
                if (passwordResult != PasswordVerificationResult.Success)
                {
                    throw new Exception($"{nameof(customer.Email)} and {nameof(customer.Password)} does not match");
                }

                TokenResult token;
                token = _tokenGenerator.Generate(customer.Id, JwtTokenGenerator.TokenType.Customer);

                GetCustomer.Response customerResponse = await _mediator.SendAsync(new GetCustomer.Request
                {
                    CustomerId = customer.Id
                });

                Response result = new Response
                {
                    Customer = customerResponse,
                    Token = token
                };

                return result;
            }

            private async Task<Customer> GetCustomer(string email)
            {
                var query = from customer in _db.Customers
                            where customer.Email == email
                            select customer;


                Customer result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }
        }
    }
}