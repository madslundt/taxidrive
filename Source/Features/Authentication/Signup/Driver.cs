using System.Threading.Tasks;
using DataModels;
using DataModels.DriverContext;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Source.Features.Driver;

namespace Source.Authentication.Signup
{
    public class SignupDriver
    {
        public class Request : IAsyncRequest<GetDriver.Response>
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(c => c).NotNull();
                RuleFor(c => c.FirstName).NotEmpty();
                RuleFor(c => c.LastName).NotEmpty();
                RuleFor(c => c.Email).NotEmpty().EmailAddress();
                RuleFor(c => c.Password.Length).GreaterThanOrEqualTo(6);
            }
        }

        public class Handler : IAsyncRequestHandler<Request, GetDriver.Response>
        {
            private readonly IPasswordHasher<Driver> _passwordHasher;
            private readonly ApplicationContext _db;
            private readonly IMediator _mediator;

            public Handler(ApplicationContext db, IPasswordHasher<Driver> passwordHasher, IMediator mediator)
            {
                _passwordHasher = passwordHasher;
                _db = db;
                _mediator = mediator;
            }

            public async Task<GetDriver.Response> Handle(Request message)
            {
                Driver driver = CreateDriver(message);
                await _db.SaveChangesAsync().ConfigureAwait(false);

                GetDriver.Response result = await _mediator.SendAsync(new GetDriver.Request
                {
                    DriverId = driver.Id
                }).ConfigureAwait(false);

                return result;
            }

            private Driver CreateDriver(Request message)
            {
                Driver driver = new Driver
                {
                    FirstName = message.FirstName,
                    LastName = message.LastName,
                    Email = message.Email
                };

                var hash = _passwordHasher.HashPassword(driver, message.Password);
                driver.Password = hash;

                _db.Drivers.Add(driver);

                return driver;
            }
        }
    }
}
