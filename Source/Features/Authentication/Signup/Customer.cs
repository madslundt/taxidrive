using System.Threading.Tasks;
using DataModels;
using DataModels.CustomerContext;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Source.Features.Customer;

namespace Source.Authentication.Signup
{
    public class SignupCustomer
    {
        public class Request : IAsyncRequest<GetCustomer.Response>
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(c => c).NotNull();
                RuleFor(c => c.FirstName).NotEmpty();
                RuleFor(c => c.LastName).NotEmpty();
                RuleFor(c => c.Email).NotEmpty().EmailAddress();
                RuleFor(c => c.Password.Length).GreaterThanOrEqualTo(6);
            }
        }

        public class Handler : IAsyncRequestHandler<Request, GetCustomer.Response>
        {
            private readonly IPasswordHasher<Customer> _passwordHasher;
            private readonly ApplicationContext _db;
            private readonly IMediator _mediator;

            public Handler(ApplicationContext db, IPasswordHasher<Customer> passwordHasher, IMediator mediator)
            {
                _passwordHasher = passwordHasher;
                _db = db;
                _mediator = mediator;
            }

            public async Task<GetCustomer.Response> Handle(Request message)
            {
                Customer customer = CreateCustomer(message);
                await _db.SaveChangesAsync().ConfigureAwait(false);

                GetCustomer.Response result = await _mediator.SendAsync(new GetCustomer.Request
                {
                    CustomerId = customer.Id
                }).ConfigureAwait(false);

                return result;
            }

            private Customer CreateCustomer(Request message)
            {
                Customer customer = new Customer
                {
                    FirstName = message.FirstName,
                    LastName = message.LastName,
                    Email = message.Email
                };

                var hash = _passwordHasher.HashPassword(customer, message.Password);
                customer.Password = hash;

                _db.Customers.Add(customer);

                return customer;
            }
        }
    }
}
