using System;
using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Source.Authentication.Signup;
using Source.Authentication.Signin;
using Microsoft.AspNetCore.Authorization;

namespace Source.Features.Customer
{
    public class CustomerController : Controller
    {
        private readonly IMediator _mediator;

        public CustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet, Route("api/customers/{companyId}")]
        public async Task<IActionResult> GetCustomer(Guid customerId)
        {
            var result = await _mediator.SendAsync(
                new GetCustomer.Request
                {
                    CustomerId = customerId
                }).ConfigureAwait(false);

            return Ok(result);
        }

        [HttpPost, AllowAnonymous, Route("api/signup/customers")]
        public async Task<IActionResult> CreateCustomer([FromBody] SignupCustomer.Request customer)
        {
            var result = await _mediator.SendAsync(
                new SignupCustomer.Request
                {
                    Email = customer.Email,
                    Password = customer.Password,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                }).ConfigureAwait(false);

            return Ok(result);
        }
        [HttpPost, AllowAnonymous, Route("api/signin/customers")]
        public async Task<IActionResult> SigninCustomer([FromBody] SigninCustomer.Request customer)
        {
            var result = await _mediator.SendAsync(
                new SigninCustomer.Request
                {
                    Email = customer.Email,
                    Password = customer.Password
                }).ConfigureAwait(false);

            return Ok(result);
        }
    }
}
