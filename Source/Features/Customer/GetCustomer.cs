using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;

namespace Source.Features.Customer
{
    public class GetCustomer
    {
        public class Response
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public DateTime Created { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid CustomerId { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.CustomerId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                Response result = await GetCustomerQuery(request.CustomerId).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"Nothing found with that {nameof(request.CustomerId)}");
                }

                return result;
            }

            private async Task<Response> GetCustomerQuery(Guid customerId)
            {
                var query = from customer in _db.Customers
                            where customer.Id == customerId
                            select new Response
                            {
                                Id = customer.Id,
                                FirstName = customer.FirstName,
                                LastName = customer.LastName,
                                Email = customer.Email,
                                Created = customer.Created
                            };

                Response result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }
        }
    }
}
