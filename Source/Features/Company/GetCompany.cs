using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using DataModels;
using MediatR;

namespace Source.Features.Company
{
    public class GetCompany
    {
        public class Response
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class Request : IAsyncRequest<Response>
        {
            public Guid CompanyId { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(query => query).NotNull();
                RuleFor(query => query.CompanyId).NotEmpty();
            }
        }

        public class Handler : IAsyncRequestHandler<Request, Response>
        {
            private readonly ApplicationContext _db;

            public Handler(ApplicationContext db)
            {
                _db = db;
            }

            public async Task<Response> Handle(Request request)
            {
                Response result = await GetCompanyQuery(request.CompanyId).ConfigureAwait(false);

                if (result == null)
                {
                    throw new ArgumentNullException($"Nothing found with that {nameof(request.CompanyId)}");
                }

                return result;
            }

            private async Task<Response> GetCompanyQuery(Guid companyId)
            {
                var query = from company in _db.Companies
                            where company.Id == companyId
                            select new Response
                            {
                                Id = company.Id,
                                Name = company.Name
                            };

                Response result = await query.FirstOrDefaultAsync().ConfigureAwait(false);

                return result;
            }
        }
    }
}
