using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MediatR;


namespace Source.Features.Company
{
    public class CompanyController : Controller
    {
        private readonly IMediator _mediator;

        public CompanyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet, Route("api/companies/{companyId}")]
        public async Task<IActionResult> GetCompany(Guid companyId)
        {
            var result = await _mediator.SendAsync(
                new GetCompany.Request
                {
                    CompanyId = companyId
                }).ConfigureAwait(false);

            return Ok(result);
        }
    }
}
