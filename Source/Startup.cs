using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DataModels;
using Microsoft.EntityFrameworkCore;
using System;
using Source.Infrastructure;
using Source.Infrastructure.Validation;
using StructureMap;
using Source.Infrastructure.MessageProcessing;
using Source.Infrastructure.Middleware;

namespace Source
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            Env = env;
        }

        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment Env { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddDbContext<ApplicationContext>(
                options => options.UseNpgsql(Configuration["DataSettings:ConnectionString"])
            );

            // services.AddConfiguredCors();
            // services.AddConfiguredTokenAuth(Configuration, Env);
            // services.AddConfiguredSwagger();

            // TODO
            // services.AddConfiguredCors();
            services.AddDistributedMemoryCache();
            // services.AddConfiguredSwagger();

            // TODO add features

            services.AddMvc();


            IContainer container = new Container();
            container.Configure(config =>
            {
                config.AddRegistry<DefaultRegistry>();
                config.AddRegistry<ValidationRegistry>();
                config.AddRegistry<MessageProcessingRegistry>();

                config.Populate(services);
            });

            return container.GetInstance<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // app.UseBrowserLink();
            }

            // app.UseAsyncExceptionHandler(loggerFactory);
            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
