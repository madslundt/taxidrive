﻿using Microsoft.IdentityModel.Tokens;

namespace Source.Infrastructure.Token
{
    public class TokenAuthOptions
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public SigningCredentials SigningCredentials { get; set; }
        public SecurityKey Key { get; set; }
    }
}