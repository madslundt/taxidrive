using System;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Source.Infrastructure.Middleware
{

    public class ErrorDto
    {
        public string Message { get; set; }
        public string Details { get; set; }

        // other fields

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }

    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context).ConfigureAwait(false);
            }
            catch (Exception ex) when (ex is FormatException || ex is ValidationException || ex is DbUpdateException)
            {
                //Bad request
                context.Response.StatusCode = 400;
                await ThrowException(ex, context);

                return;
            }
            catch (Exception ex) when (ex is ArgumentNullException)
            {
                //Not found
                context.Response.StatusCode = 404;
                await ThrowException(ex, context);

                return;
            }
            catch (Exception ex) when (ex is SecurityException)
            {
                //Forbidden
                context.Response.StatusCode = 403;
                await ThrowException(ex, context);

                return;
            }
            catch (Exception ex) when (ex is DbUpdateException)
            {
                //Conflict
                context.Response.StatusCode = 409;
                await ThrowException(ex, context);

                return;
            }
            catch (Exception ex)
            {
                //Internal server error
                context.Response.StatusCode = 500;
                await ThrowException(ex, context);

                throw;
            }
        }

        private async Task ThrowException(Exception ex, HttpContext context)
        {
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(new ErrorDto()
            {
                Message = ex.Message,
                Details = ex.ToString()
            }.ToString(), Encoding.UTF8);
        }
    }
}