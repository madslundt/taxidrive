using System;
using System.Collections.Generic;
using System.Security;
using System.Threading.Tasks;
using AsyncFriendlyStackTrace;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Praice.Api.Infrastructure.Logging
{

    public class AsyncLoggingHandler<TRequest, TResponse> : IAsyncRequestHandler<TRequest, TResponse> where TRequest : IAsyncRequest<TResponse>
    {
        private readonly IAsyncRequestHandler<TRequest, TResponse> _innerHander;
        private readonly IHttpContextAccessor _http;
        private readonly ILoggerFactory _logger;

        public AsyncLoggingHandler(IAsyncRequestHandler<TRequest, TResponse> innerHandler, IHttpContextAccessor http, ILoggerFactory logger)
        {
            _logger = logger;
            _http = http;
            _innerHander = innerHandler;
        }

        public async Task<TResponse> Handle(TRequest request)
        {
            TResponse response = default(TResponse);
            try
            {
                response = await _innerHander.Handle(request).ConfigureAwait(false);
            }

            catch (Exception ex) when (ex is FormatException || ex is ValidationException)
            {
                var logger = _logger.CreateLogger(ex.GetType());
                if (!_http.HttpContext.User.Identity.IsAuthenticated)
                {
                    logger.LogInformation("User failed a validation and exception of type {Type} was thrown with message {Message}. RequestInfo: {@Request}. Full exception: {@Exception}",
                        ex.GetType(),
                        ex.Message,
                        request,
                        ex.ToAsyncString());

                    throw;
                }
                else
                {
                    logger.LogInformation("User with {Id} failed a validation, and exception of type {Type} was thrown with message {Message}. RequestInfo: {@Request}. Full exception: {@Exception}",
                        // _http.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier),
                        ex.GetType(),
                        ex.Message,
                        request,
                        ex.ToAsyncString());
                }

                throw;
            }
            catch (Exception ex) when (ex is DbUpdateException)
            {
                var logger = _logger.CreateLogger(ex.GetType());
                logger.LogInformation("User tried to create entity that already existed, and exception of type {Type} was thrown with message {Message}. RequestInfo: {@Request}. Full exception: {@Exception}",
                    ex.GetType(),
                    ex.Message,
                    request,
                    ex.ToAsyncString());

                throw;
            }
            catch (Exception ex) when (ex is SecurityException)
            {
                var logger = _logger.CreateLogger(ex.GetType());
                logger.LogInformation("User tried to access a method they did not have access to, and exception of type {Type} was thrown with message {Message}. RequestInfo: {@Request}. Full exception: {@Exception}",
                    ex.GetType(),
                    ex.Message,
                    request,
                    ex.ToAsyncString());

                throw;
            }
            catch (Exception ex) when (ex is ArgumentNullException)
            {
                var logger = _logger.CreateLogger(ex.GetType());

                logger.LogInformation(@"Variable was null {Variable} with message {Message}. RequestInfo {@Request} Affected User: {UserId}. Full exception: {@Exception}",
                    ex.Data,
                    ex.Message,
                    request,
                    // _http.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier),
                    ex.ToAsyncString());

                throw;
            }
            catch (Exception ex)
            {
                var logger = _logger.CreateLogger(ex.GetType());
                logger.LogCritical("Unhandled exception of type {Type} was thrown with message: {Message}. RequestInfo was {@Request}. Affected user {UserId}. Http: {@HttpContext}. Full exception: {@Exception}",
                    ex.GetType(),
                    ex.Message,
                    request,
                    // _http.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier),
                    _http.HttpContext,
                    ex.ToAsyncString());

                IDictionary<string, string> properties = new Dictionary<string, string>
                {
                    { "Exception Message", ex.Message },
                    { "StackTrace", ex.StackTrace }
                };

                // var t = new Microsoft.ApplicationInsights.TelemetryClient();
                // t.TrackException(ex, properties);

                throw;
            }

            return response;
        }
    }
}
