using StructureMap;
using FluentValidation;
using Source.Features.Company;
using Source.Authentication.Signin;
using Source.Authentication.Signup;

namespace Source.Infrastructure.Validation
{
    public class ValidationRegistry : Registry
    {
        public ValidationRegistry()
        {
            // Company
            For<IValidator<GetCompany.Request>>().Use<GetCompany.RequestValidator>().Transient();

            // Customer
            For<IValidator<SigninCustomer.Request>>().Use<SigninCustomer.RequestValidator>().Transient();
            For<IValidator<SignupCustomer.Request>>().Use<SignupCustomer.RequestValidator>().Transient();

            // Driver
            For<IValidator<SigninDriver.Request>>().Use<SigninDriver.RequestValidator>().Transient();
            For<IValidator<SignupDriver.Request>>().Use<SignupDriver.RequestValidator>().Transient();
        }
    }
}