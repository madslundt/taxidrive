using FluentValidation;
using MediatR;
using Source.Infrastructure.Validation;
using StructureMap;

namespace Source.Infrastructure.MessageProcessing
{

    public class MessageProcessingRegistry : Registry
    {
        public MessageProcessingRegistry()
        {
            Scan(scanner =>
            {
                scanner.AssemblyContainingType<IMediator>();
                scanner.AssemblyContainingType<MessageProcessingRegistry>();

                scanner.AddAllTypesOf(typeof(IAsyncRequestHandler<,>));
                scanner.AddAllTypesOf(typeof(IValidator<>));


                scanner.WithDefaultConventions();
            });

            For<SingleInstanceFactory>().Use<SingleInstanceFactory>(ctx => t => ctx.GetInstance(t));
            For<MultiInstanceFactory>().Use<MultiInstanceFactory>(ctx => t => ctx.GetAllInstances(t));

            var asynchandlerType = For(typeof(IAsyncRequestHandler<,>));
            asynchandlerType.DecorateAllWith(typeof(AsyncValidatorHandler<,>));
            // asynchandlerType.DecorateAllWith(typeof(AsyncLoggingHandler<,>));
        }
    }
}