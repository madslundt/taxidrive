using DataModels.CustomerContext;
using DataModels.DriverContext;
using Microsoft.AspNetCore.Identity;
using Source.Features.Authentication.Token;
using StructureMap;

namespace Source.Infrastructure
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            For<IPasswordHasher<Customer>>().Use<PasswordHasher<Customer>>().Singleton();
            For<IPasswordHasher<Driver>>().Use<PasswordHasher<Driver>>().Singleton();

            For<ITokenGenerator>().Use<JwtTokenGenerator>().Singleton();
            For<ITokenValidator>().Use<JwtTokenValidator>().Singleton();
        }
    }
}